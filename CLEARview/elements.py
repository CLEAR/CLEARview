# -*- coding: utf-8 -*-

import numpy as np
import CLEARview.opticsUtils

class Element(object):
    "The default implementation."
    
    length = 0.0 #[m]
    canSubdivide = False
    isLinear = False
    
    typeName = 'Element'
    
    def computeTransform(self, x,y=None):
        """
        Transform one or more particles through the element.
        The particles are given as a numpy array of size (2,numpart),
        units [mm], [rad].
        
        Note that it is possible to use 2D tracking only.
        """
        
        # Default implementation is a No-OP
        if y is None:
            return(x)
        else:
            return (x,y)

    def computeSubdividedTransform(self, z, x,y=None):
        """
        Transform one or more particles partially through the element.
        How far is controlled by z [m]
        """
        assert self.canSubdivide
        assert z <= self.length
        
        # No default implementation.
        raise NotImplementedError
        
    def computeTwissTransform(self, x,y=None):
        """
        Transforms the given twiss parameters through the element.
        Each x,y are given as a numpy array of size 3,
        where the elements are beta/alpha/gamma.
        
        Units:
            beta  : [m]
            alpha : [-]
            gamma : [1/m]
            
        
        Note that it is possible to use 2D optics only.
        
        Note: For non-linear elements, it would need to know the position as well.
        This is currently not supported.
        """

        #Default implementation is just the identity transform, i.e. a marker
        if y is None:
            return x
        else:
            return (x,y)

    def computeSubdividedTwissTransform(self, z, x,y=None):
        """
        Transform one or more twiss parameters partially through the element.
        """
        assert self.canSubdivide
        assert z <= self.length
        # No default implementation.
        raise NotImplementedError

    def getTransform(self):
        """
        Return the matrix which transforms the (x,x')^T vector through the element.
        Only valid for linear elements
        """
        if not self.isLinear:
            raise NonLinearError

        #Default implementation is just the identity transform
        return(np.eye(2), np.eye(2))

    def getTwissTransform(self):
        """
        Return the matrix which transforms the (beta,alpha,gamma')^T vector through the element.
        Only valid for linear elements
        """
        if not self.isLinear:
            raise NonLinearError(self.__class__.__name__)

        x,y = self.getTransform()
        xm = CLEARview.opticsUtils.twissTransformMatrix(x)
        ym = CLEARview.opticsUtils.twissTransformMatrix(y)
        return (xm,ym);

class Marker(Element):
    """
    Same as a pure element,
    but a different class so that it can be separated out for plotting etc.
    """
    isLinear = True
    
    typeName = 'Marker'

class Drift(Element):
    isLinear     = True
    canSubdivide = True
    
    typeName = 'Drift'

    driftMatrix = None
    driftMatrixTwiss = None
    
    def __init__(self,L):
        self.length = L
        
        self.driftMatrix      = self.makeDriftMatrix(self.length)
        self.driftMatrixTwiss = CLEARview.opticsUtils.twissTransformMatrix(self.driftMatrix)
    
    def makeDriftMatrix(self,L):
        return np.asanyarray([\
                              [1.0, L   ],\
                              [0.0, 1.0 ]\
                             ])

    def getTransform(self):
        return (self.driftMatrix, self.driftMatrix);
        
    def computeTransform(self, x,y=None):
        x1     = np.matmul(self.driftMatrix,x)
        if y is None:
            return x1
        y1 = np.matmul(self.driftMatrix,y)
        return (x1,y1)
    
    def computeSubdividedTransform(self, z, x,y=None):
        assert z < self.length
        
        partialDriftMatrix = self.makeDriftMatrix(z)
        
        x1 = np.matmul(partialDriftMatrix,x)
        if y is None:
            return x1
        y1 = np.matmul(partialDriftMatrix,y)
        return (x1,y1)

    def computeTwissTransform(self, x,y=None):
        x1 = np.matmul(self.driftMatrixTwiss,x)
        if y is None:
            return x1
        y1 = np.matmul(self.driftMatrixTwiss,y)
        return (x1,y1)
    
    def computeSubdividedTwissTransform(self, z, x,y=None):
        assert self.canSubdivide  
        assert z <= self.length

        partialDriftMatrix      = self.makeDriftMatrix(z)
        partialDriftMatrixTwiss = CLEARview.opticsUtils.twissTransformMatrix(partialDriftMatrix)

        x1 = np.matmul(partialDriftMatrixTwiss,x)
        if y is None:
            return x1
        y1 = np.matmul(partialDriftMatrixTwiss,y)
        return (x1,y1)

class QuadThin(Element):
    "Thin quadrupole kick"
    isLinear=True
    canSubdivide = False

    typeName = 'QuadThin'
    
    _fCutoff = 1000.0 #F value where the quad becomes a marker [m]

    f = None #Focal length [m]

    matrix_x = None
    matrix_y = None
    twissMatrix_x = None
    twissMatrix_y = None

    def __init__(self,f):
        QuadThin.update(self,f)

    def update(self,f):
        self.f = f

        if abs(f) > self._fCutoff:
            self.matrix_x = np.eye(2)
            self.matrix_y = np.eye(2)

            self.twissMatrix_x = np.eye(3)
            self.twissMatrix_y = np.eye(3)
        else:
            self.matrix_x = self.makeFocusMatrix(f)
            self.matrix_y = self.makeFocusMatrix(-f)

            self.twissMatrix_x = CLEARview.opticsUtils.twissTransformMatrix(self.matrix_x)
            self.twissMatrix_y = CLEARview.opticsUtils.twissTransformMatrix(self.matrix_y)

    def makeFocusMatrix(self,f):
        """
        Create a matrix corresponding to a focusing quadrupole;
        f < 0 for defocusing.
        """
        assert abs(f) < self._fCutoff

        return np.asarray([\
                           [1.0   , 0.0],\
                           [-1.0/f, 1.0] \
                           ])

    def getTransform(self):
        return(self.matrix_x,self.matrix_y)

    def computeTransform(self,x,y=None):
        x1 = np.matmul(self.matrix_x,x)
        if y is None:
            return x1
        y1 = np.matmul(self.matrix_x,y)
        return(x1,y1)
    def computeTwissTransform(self,x,y=None):
        x1 = np.matmul(self.twissMatrix_x,x)
        if y is None:
            return x1
        y1 = np.matmul(self.twissMatrix_x,y)
        return(x1,y1)

class QuadThick(Element):
    "Thick quadrupole kick"
    isLinear     = True
    canSubdivide = True
    
    typeName = 'QuadThick'
    
    _kCutoff = 1e-10 #k value where the quad becomes a drift [1/m^2]
    driftElement = None    # Internal drift element, used in cases where abs(k) < _kCutoff
    
    k = None
    
    matrix_x = None
    matrix_y = None
    
    twissMatrix_x = None
    twissMatrix_y = None
    
    def __init__(self, L, k):
        self.length = L
        QuadThick.update(self,k)
        
    def update(self,k):
        self.k = k
        
        if abs(k) < self._kCutoff:
            self.driftElement = Drift(self.length)
        elif k > 0:
            #Focusing quad
            self.matrix_x = self.makeFocusMatrix   ( self.k, self.length)
            self.matrix_y = self.makeDefocusMatrix (-self.k, self.length)
        elif k < 0:
            #Defocusing quad
            self.matrix_y = self.makeFocusMatrix   (-self.k, self.length)
            self.matrix_x = self.makeDefocusMatrix ( self.k, self.length)
        
        if abs(k) > self._kCutoff:
            self.twissMatrix_x = CLEARview.opticsUtils.twissTransformMatrix(self.matrix_x)
            self.twissMatrix_y = CLEARview.opticsUtils.twissTransformMatrix(self.matrix_y)
        else:
            self.twissMatrix_x = None
            self.twissMatrix_y = None

    def makeFocusMatrix(self, k,L):
        assert k > self._kCutoff
        
        return np.asarray([\
                           [            np.cos(L*np.sqrt(k)), np.sin(L*np.sqrt(k))/np.sqrt(k)],\
                           [-np.sqrt(k)*np.sin(L*np.sqrt(k)), np.cos(L*np.sqrt(k))           ]\
                           ])
    
    def makeDefocusMatrix(self, k,L):
        assert k < self._kCutoff
        
        return np.asarray([\
                           [            np.cosh(L*np.sqrt(-k)), np.sinh(L*np.sqrt(-k))/np.sqrt(-k)],\
                           [np.sqrt(-k)*np.sinh(L*np.sqrt(-k)), np.cosh(L*np.sqrt(-k))            ]\
                           ])

    def getTransform(self):
        if abs(self.k) < self._kCutoff:
            return self.driftElement.getTransform();
        return (self.matrix_x, self.matrix_y)
    
    def computeTransform(self, x,y=None):
        if abs(self.k) < self._kCutoff:
            return self.driftElement.computeTransform(x,y)
        
        x1 = np.matmul(self.matrix_x,x)
        if y is None:
            return x1
        y1 = np.matmul(self.matrix_y,y)
        return (x1,y1)
    
    def computeSubdividedTransform(self,z, x,y=None):
        assert z > 0 and z < self.length
        
        if abs(self.k) < self._kCutoff:
            return self.driftElement.computeSubdividedTransform(z,x,y)
        
        elif self.k > 0:
            #Focusing quad
            partialMatrix_x = self.makeFocusMatrix   ( self.k, z)
            x1 = np.matmul(partialMatrix_x,x)
            if y is None:
                return x1
            partialMatrix_y = self.makeDefocusMatrix ( -self.k, z)
            y1 = np.matmul(partialMatrix_y,y)
            return (x1,y1)
        
        elif self.k < 0:
            #Defocusing quad
            partialMatrix_x = self.makeDefocusMatrix ( self.k, z)
            x1 = np.matmul(partialMatrix_x,x)
            if y is None:
                return x1
            
            partialMatrix_y = self.makeFocusMatrix   (-self.k, z)
            y1 = np.matmul(partialMatrix_y,y)
            return (x1,y1)
    
    def computeTwissTransform(self, x,y=None):
        if abs(self.k) < self._kCutoff:
            return self.driftElement.computeTwissTransform(x,y)
        
        x1    = np.matmul(self.twissMatrix_x,x)

        if y is None:
            return x1
        y1 = np.matmul(self.twissMatrix_y,y)
        return (x1,y1)
    
    def computeSubdividedTwissTransform(self,z, x,y=None):
        assert z > 0 and z < self.length
        
        if abs(self.k) < self._kCutoff:
            return self.driftElement.computeSubdividedTwissTransform(z,x,y)
        
        elif self.k > 0:
            #Focusing quad
            partialMatrix_x = self.makeFocusMatrix ( self.k, z )
            partialTwissMatrix_x = CLEARview.opticsUtils.twissTransformMatrix( partialMatrix_x )
            x1 = np.matmul(partialTwissMatrix_x,x)
            if y is None:
                return x1
            
            partialMatrix_y = self.makeDefocusMatrix ( -self.k, z )
            partialTwissMatrix_y = CLEARview.opticsUtils.twissTransformMatrix( partialMatrix_y )
            y1 = np.matmul(partialTwissMatrix_y,y)
            return (x1,y1)
        
        elif self.k < 0:
            #Defocusing quad
            partialMatrix_x = self.makeDefocusMatrix ( self.k, z)
            partialTwissMatrix_x = CLEARview.opticsUtils.twissTransformMatrix( partialMatrix_x )
            x1 = np.matmul(partialTwissMatrix_x,x)
            if y is None:
                return x1
            
            partialMatrix_y = self.makeFocusMatrix ( -self.k, z )
            partialTwissMatrix_y = CLEARview.opticsUtils.twissTransformMatrix( partialMatrix_y )
            y1 = np.matmul(partialTwissMatrix_y,y)
            return (x1,y1)
    
class KickerThick(Element):
    isLinear     = True
    canSubdivide = True
    
    typeName = 'KickerThick'
    
    ang_x = None # Kick angle in x [rad]
    ang_y = None # Kick angle in x [rad]
    
    kickVector_x = None
    kickVector_y = None

    drift = None
    
    def __init__(self, L, ang_x, ang_y=None):
        self.length = L
        self.drift = Drift(self.length)
        KickerThick.update(self, ang_x, ang_y)
    
    def update(self, ang_x, ang_y=None):

        self.ang_x = ang_x
        self.ang_y = ang_y

        self.kickVector_x = np.asarray([0,ang_x])
        self.kickVector_y = np.asarray([0,ang_y])

    def getTransform(self):
        return self.drift.getTransform()

    def computeTransform(self, x,y=None):
        #Same transform as used in Mad-X twiss.f90 :: tmcorr()
        
        if y is None:
            assert self.ang_y is None
        else:
            assert not self.ang_y is None
        
        #1. Half-angle kick
        x1 = (x.T + self.kickVector_x/2.0).T
        if not y is None:
            y1 = (y.T + self.kickVector_y/2.0).T
        
        #2. Drift
        (x2,y2) = self.drift.computeTransform(x1,y1)
        
        #3. Half-angle kick again
        x3 = (x2.T + self.kickVector_x/2.0).T
        if not y is None:
            y3 = (y2.T + self.kickVector_y/2.0).T
        
        #Return!
        if y is None:
            return x3
        return (x3,y3)

    def computeTwissTransform(self, x,y=None):
        return self.drift.computeTwissTransform(x,y)
    def computeSubdividedTransform(self, z, x,y=None):
        assert (z > 0.0) and (z < self.length)
        #1. Half-angle kick
        x1 = (x.T + self.kickVector_x/2.0).T
        if not y is None:
            y1 = (y.T + self.kickVector_y/2.0).T
            
        return self.drift.computeSubdividedTransform(z, x1,y1)
    
    def computeSubdividedTwissTransform(self, z,x,y):
        return self.drift.computeSubdividedTwissTransform(z, x,y)
    
class CLEAR_Q(QuadThick):
    "CLEAR-type thick quadrupole"
        
    current = None # [A]
    P0      = None # [MeV/c]
    
    L_quad = 0.226 # [m] magnetic length of quadrupole
    
    def __init__(self, I, P0):
        self.current = I
        super().__init__(self.L_quad, 0.0)
        self.update(current=I, P0=P0)

    def update(self,current=None,P0=None):
        assert not (current is None and P0 is None)
        if not current is None:
            self.current = current
        if not P0 is None:
            self.P0 = P0
        
        k = self.compute_k()
        QuadThick.update(self,k)

    def compute_k(self):
        "Focusing strength of CLEAR quadrupoles in 1/m**2 given current [A]"
        EC_QD = 0.056; # excitation constant dB/dx/I = 0.056 [T/Am]
        F_QD = CLEARview.opticsUtils.SI_c * 1e-6 * EC_QD;
        return self.current * F_QD / self.P0;
    

        
class CLEAR_DG120(KickerThick):
    "G-type corrector with 120 windings for CLEAR"
    
    current_x = None  # [A]
    current_y = None  # [A]
    P0        = None  # [MeV/c]

    L_corr    = 0.164 # [m] magnetic length of corrector

    def __init__(self, Ix,Iy, P0):
        KickerThick.__init__(self, self.L_corr, 0.0,0.0)
        self.update(Ix,Iy,P0)

    def update(self,Ix=None,Iy=None,P0=None):
        assert not (Ix is None and Iy is None and P0 is None)

        if not P0 is None:
            self.P0 = P0
        assert not self.P0 is None

        ang_x = self.ang_x
        if not Ix is None:
            self.current_x = Ix
            ang_x = self.computeAng(Ix, self.P0)

        ang_y = self.ang_y
        if not Iy is None:
            self.current_y = Iy
            ang_y = self.computeAng(Iy, self.P0)

        KickerThick.update(self,ang_x,ang_y)

    def computeAng(self,I,P0):
        # B(I)  = 13.3 Gauss/A
        kg_corrcallib = self.L_corr * 0.00133/(3.3356/1000);

        return I*kg_corrcallib/P0

class CLEAR_DJ(KickerThick):
    "J-type corrector for CLEAR"

    current_x  = None  # [A]
    current_y  = None  # [A]
    P0         = None  # [MeV/c]

    # Values from
    # http://ctf3-tbts.web.cern.ch/ctf3-tbts/beams/Rapport_correcteur_DJ.pdf
    L_corr     = 0.248  # [m] magnetic length of corrector
    kappa_corr = 0.0018 #[T/A] B(I) for corrector

    def __init__(self, Ix,Iy, P0):
        KickerThick.__init__(self, self.L_corr, 0.0,0.0)
        self.update(Ix,Iy,P0)

    def update(self,Ix=None,Iy=None,P0=None):
        assert not (Ix is None and Iy is None and P0 is None)

        if not P0 is None:
            self.P0 = P0
        assert not self.P0 is None

        ang_x = self.ang_x
        if not Ix is None:
            self.current_x = Ix
            ang_x = self.computeAng(Ix, self.P0)

        ang_y = self.ang_y
        if not Iy is None:
            self.current_y = Iy
            ang_y = self.computeAng(Iy, self.P0)

        KickerThick.update(self,ang_x,ang_y)

    def computeAng(self,I,P0):
        return I * self.L_corr * self.kappa_corr * \
            (CLEARview.opticsUtils.SI_c/(1e6*P0))

#Plasma lens
class PlasmaLensUtilities(object):
    @staticmethod
    def computeGradient(current,radius):
        "Utility function to convert current [A] and radius [mm] to gradient [T/m]"
        gradient = CLEARview.opticsUtils.mu0 * current / (2*np.pi*(radius*1e-3)**2) # [T/m]
        return gradient
    @staticmethod
    def computeCurrent(gradient,radius):
        "Utility function to convert gradient [T/m] and radius [mm] to current [A]"
        current = gradient * (2*np.pi*(radius*1e-3)**2) / CLEARview.opticsUtils.mu0
        return current
    @staticmethod
    def computePlasmaK(current, radius, P0_MeV):
        "Utility function to compute the focusing strength k[1/m^2] of a plasma lens with a given current[A] and radius[mm]"
        gradient = CLEAR_PlasmaLens.computeGradient(current,radius)
        return gradient*CLEARview.opticsUtils.SI_c/(P0_MeV*1e6)

class CLEAR_PlasmaLens(QuadThick,PlasmaLensUtilities):
    r = None
    canSubdivide = True
    def __init__(self,L,r,I, P0):
        """
        Setup a linear plasma lens given the length [m],
        radius [mm], current [A], and beam momentum [MeV/c]
        """
        CLEARview.elements.QuadThick.__init__(self,L,0.0)
        self.r = r
        self.update(I,P0)

    def update(self,I=None,P0=None):
        assert not (I is None and P0 is None)
        if not P0 is None:
            self.P0 = P0
        if not I is None:
            self.I = I
            self.k = self.computePlasmaK(self.I, self.r, self.P0)
            if abs(self.k) < self._kCutoff:
                self.driftElement=CLEARview.elements.Drift(self.length)
            elif self.k > 0:
                #Focusing
                self.matrix_x = self.makeFocusMatrix (self.k,self.length)
                self.matrix_y = self.matrix_x
            elif self.k < 0:
                #Defocusing
                self.matrix_x = self.makeDefocusMatrix (self.k,self.length)
                self.matrix_y = self.matrix_x

            if abs(self.k) > self._kCutoff:
                self.twissMatrix_x = CLEARview.opticsUtils.twissTransformMatrix(self.matrix_x)
                self.twissMatrix_y = CLEARview.opticsUtils.twissTransformMatrix(self.matrix_y)



    def computeSubdividedTransform(self,z, x,y=None):
        assert z > 0 and z < self.length

        if abs(self.k) < self._kCutoff:
            return self.driftElement.computeSubdividedTransform(z,x,y)

        elif self.k > 0:
            #Focusing quad
            partialMatrix = self.makeFocusMatrix   ( self.k, z)
            x1 = np.matmul(partialMatrix,x)
            if y is None:
                return x1
            y1 = np.matmul(partialMatrix,y)
            return (x1,y1)

        elif self.k < 0:
            #Defocusing quad
            partialMatrix = self.makeDefocusMatrix ( self.k, z)
            x1 = np.matmul(partialMatrix,x)
            if y is None:
                return x1
            y1 = np.matmul(partialMatrix,y)
            return (x1,y1)

    def computeSubdividedTwissTransform(self,z, x,y=None):
        assert z > 0 and z < self.length

        if abs(self.k) < self._kCutoff:
            return self.driftElement.computeSubdividedTwissTransform(z,x,y)

        elif self.k > 0:
            #Focusing quad
            partialMatrix = self.makeFocusMatrix ( self.k, z )
            partialTwissMatrix = CLEARview.opticsUtils.twissTransformMatrix( partialMatrix )
            x1 = np.matmul(partialTwissMatrix,x)
            if y is None:
                return x1
            y1 = np.matmul(partialTwissMatrix,y)
            return (x1,y1)

        elif self.k < 0:
            #Defocusing quad
            partialMatrix = self.makeDefocusMatrix ( self.k, z)
            partialTwissMatrix = CLEARview.opticsUtils.twissTransformMatrix( partialMatrix )
            x1 = np.matmul(partialTwissMatrix,x)
            if y is None:
                return x1
            y1 = np.matmul(partialTwissMatrix,y)
            return (x1,y1)

class PlasmaLens_Thin(QuadThin,PlasmaLensUtilities):
    """
    Setup a linear thin plasma lens given the focal length
    """
    canSubdivide = False

    def __init__(self,f):
        QuadThin.__init__(self,f)
        self.update(f)

    def update(self,f):
        self.f = f

        if abs(f) > self._fCutoff:
            self.matrix_x = np.eye(2)
            self.matrix_y = np.eye(2)

            self.twissMatrix_x = np.eye(3)
            self.twissMatrix_y = np.eye(3)
        else:
            self.matrix_x = self.makeFocusMatrix(f)
            self.matrix_y = self.makeFocusMatrix(f)

            self.twissMatrix_x = CLEARview.opticsUtils.twissTransformMatrix(self.matrix_x)
            self.twissMatrix_y = CLEARview.opticsUtils.twissTransformMatrix(self.matrix_y)

## EXCEPTIONS ##
class NonLinearError(Exception):
    pass
