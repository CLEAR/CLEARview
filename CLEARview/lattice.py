# -*- coding: utf-8 -*-

import collections
import numpy as np

import CLEARview.elements

class Lattice():
    "Stores and manipulates a squence of elements, and computes stuff using it."
    
    #List of elements -- this is an OrderedDict, sorted by s position.
    # The key is the element name, the value is the element object.
    elements = None
    
    #One can also build lattices using implicit drifts,
    # i.e. first push a list of non-overlapping elements,
    # then afterwards construct self.elements
    implicitElems_s    = None # s-position of the middle of the element [m]
    implicitElems_elem = None # The elements
    implicitElems_name = None # The element names
    
    __placement_tol    = 1e-6 # Placement tolerance
                              # how close can it be before we will make a drift [m]
    
    def __init__(self):
        self.elements = collections.OrderedDict()
    
    def addElem(self, element, name):
        if not (self.implicitElems_elem is None):
            raise ImplicitNotFinalizedError
        if type(name) != str:
            raise TypeError("Input argument 'name' must be of type str, got " + str(type(name)))
        if not isinstance(element, CLEARview.elements.Element):
            raise TypeError("Input argument 'element'must be a subclass of CLEARview.elements.Element")
        
        #Note: 'name' argument comes last as I want to make this an optional
        # argument in the future, making automatic names like "drift1", "quad42" etc.
        if name in self.elements.keys():
            raise ElementOverwriteError
            
        self.elements[name] = element
    
    # Functions for building the sequence using disjointed active elements,
    # then automatically building the drifts later
    def implicitSeq_addElem(self, element, name, s, refer='centre', referFrom=None):
        if len(self.elements) != 0:
            raise ImplicitAlreadyFinalizedError
        
        if self.implicitElems_elem == None:
            self.implicitElems_s    = []
            self.implicitElems_elem = []
            self.implicitElems_name = []
        
        if name in self.implicitElems_name:
            raise ElementOverwriteError
        
        s0 = None #Centre position [m]
        if refer == 'centre':
            s0 = s
        elif refer == 'start':
            s0 = s+element.length/2.0
        elif refer == 'end':
            s0 = s-element.length/2.0
        else:
            raise ValueError("'refer' must be either 'centre', 'start','end', got '{}'".format(refer))
        
        if not referFrom is None:
            assert type(referFrom) == str
            
            refPoint = None
            if referFrom.endswith(".end"):
                fromName = referFrom[:-4]
                refPoint = 'e'
            elif referFrom.endswith(".centre"):
                fromName = referFrom[:-7]
                refPoint = 'c'
            elif referFrom.endswith(".start"):
                fromName = referFrom[:-6]
                refPoint = 's'
            else:
                fromName = referFrom
                refPoint = 'c'
            
            #find the reference point
            foundRefIdx = None
            for idx in range(len(self.implicitElems_name)):
                if fromName == self.implicitElems_name[idx]:
                    foundRefIdx = idx
                    break
            if foundRefIdx is None:
                raise KeyError("referFrom='{}' / fromName={} was not found".format(referFrom, fromName))
        
            s0 += self.implicitElems_s[foundRefIdx]
            if refPoint == 'e':
                s0 += self.implicitElems_elem[foundRefIdx].length/2.0
            elif refPoint == 's':
                s0 -= self.implicitElems_elem[foundRefIdx].length/2.0
                
        self.implicitElems_s.append(s0)
        self.implicitElems_elem.append(element)
        self.implicitElems_name.append(name)
    
    def implicitSeq_removeElem(self,name):
        if len(self.elements) != 0:
            raise ImplicitAlreadyFinalizedError
        if not name in self.implicitElems_name:
            raise KeyError
        
        deleteIdx = None
        for idx in range(len(self.implicitElems_name)):
            if self.implicitElems_name[idx] == name:
                deleteIdx = idx
                break

        del self.implicitElems_s[deleteIdx]
        del self.implicitElems_elem[deleteIdx]
        del self.implicitElems_name[deleteIdx]
        
    def implicitSeq_addDriftSplit(self,s):
        "Split the drifts at a the given point, effectively adding a marker"
        assert len(self.elements) == 0
        if self.implicitElems_elem is None:
            raise ImplicitAlreadyFinalizedError
    
        raise NotImplementedError
        
    def implicitSeq_finalize(self):
        "Call this when done inserting implicit elements to build a 'real' sequence"
        assert len(self.elements) == 0
        if self.implicitElems_elem is None:
            raise ImplicitAlreadyFinalizedError
    
        argsort_s = np.argsort(self.implicitElems_s)
        s0 = 0 #exit of previous element
        numDrifts = 0
        for idx in argsort_s:
            ds = (self.implicitElems_s[idx] - self.implicitElems_elem[idx].length/2.0) - s0
            if abs(ds) > self.__placement_tol:
                #We need to make a drift
                if ds < 0.0:
                    print()
                    print("Elements so far:")
                    sElStart = 0.0
                    for (k,v) in self.elements.items():
                        print(k, ": type '"+v.typeName+"', sStart=", sElStart, "length=",v.length)
                        sElStart += v.length
                    print("See exception for info on element we're now trying to load...")
                    raise NegativeDriftError(f"ds={ds} [m], before {self.implicitElems_name[idx]}, s={self.implicitElems_s[idx]} [m], L={self.implicitElems_elem[idx].length} [m], sStart={self.implicitElems_s[idx] - self.implicitElems_elem[idx].length/2.0} [m]")
                
                newDrift = CLEARview.elements.Drift(ds)
                newDrift_name = "drift_{}".format(numDrifts)
                numDrifts += 1
                
                self.elements[newDrift_name] = newDrift
                
            self.elements[self.implicitElems_name[idx]] = \
                self.implicitElems_elem[idx]
            s0 = self.implicitElems_s[idx] + self.implicitElems_elem[idx].length/2.0

        self.implicitElems_s    = None
        self.implicitElems_elem = None
        self.implicitElems_name = None
    
    def getElemS(self, name, atEnd=False):
        """
        Return the s-position of the start (or if atEnd=True, the end) of the named element.
        Works for both implicit (non-finalized) and explicit elements.
        """
        if self.implicitElems_elem is None:
            s = 0.0
            for k,v in self.elements.items():
                if k == name:
                    if atEnd:
                        s += v.length
                    return s
                s += v.length
            
        else:
            for idx in range(len(self.implicitElems_name)):
                if self.implicitElems_name[idx] == name:
                    if atEnd:
                        return self.implicitElems_s[idx] + self.implicitElems_elem[idx].length/2.0
                    else:
                        return self.implicitElems_s[idx] - self.implicitElems_elem[idx].length/2.0
        
        #Fall through to this error
        raise KeyError("Did not find element named '"+name+"'")
    
    def getTwissIdx(self,name):
        """
        Returns the index into the xt,yt, etc. arrays given the element name.
        The lattice must be finalized before using this function.
        """
        
        if not (self.implicitElems_elem is None):
            raise ImplicitNotFinalizedError

        if name == 'START':
            return 0
        
        keys = list(self.elements.keys())
        for idx in range(len(keys)):
            if keys[idx] == name:
                return idx+1
        
        raise KeyError
        
    def getSubLattice(self, firstElement=None, lastElement=None, deepCopy=False):
        """
        Return a new lattice starting with firstElement and ending with lastElement (inclusive).
        If not setting firstElement or lastElement, the first/last element of the original lattice will be used.
        The new lattice will be a shallow copy of the old one, i.e. they will share elements which contain state.
        The lattice must be finalized before using this function.
        """
        if not (self.implicitElems_elem is None):
            raise ImplicitNotFinalizedError
        
        newLattice = Lattice()
        
        keys = list(self.elements.keys())
        
        startIdx = 0
        if firstElement != None:
            startIdx = None
            for idx in range(len(keys)):
                if keys[idx] == firstElement:
                    startIdx = idx
                    break
            if startIdx == None:
                raise KeyError("Did not find firstElement = '" + firstElement + "'")
        
        stopIdx = len(keys) # points to the element after the final one
        if lastElement != None:
            stopIdx = None
            for idx in range(len(keys)):
                if keys[idx] == lastElement:
                    stopIdx = idx+1
                    break
            if stopIdx == None:
                raise KeyError("Did not find lastElement = '" + lastElement + "'")
    
        if startIdx > (stopIdx-1):
            raise KeyError("firstElement is after lastElement")
        
        for idx in range(startIdx,stopIdx):
            k = keys[idx]
            v = self.elements[k]
            newLattice.addElem(v,k)
        
        return newLattice
        
    def computeTrajectory(self, x0, y0=None, subdivide=None):
        if not (self.implicitElems_elem is None):
            raise ImplicitNotFinalizedError

        s = []
        x = []
        y = None
        
        s.append(0.0)
        x.append(x0)
        if y0 is not None:
            y = []
            y.append(y0)
        
        #For slicing in case subdivide is switched on
        sSub = None
        xSub = None
        ySub = None
        if not subdivide is None:
            assert type(subdivide) == float
            assert subdivide > 0
            
            sSub = []
            sSub.append(0.0)
            xSub = []
            xSub.append(x0)
            if not y0 is None:
                ySub = []
                ySub.append(y0)
        
        #Compute trajectory
        for k,v in self.elements.items():
            s.append(v.length+s[-1])
            
            if y0 is None:
                x1 = v.computeTransform(x[-1])
                x.append(x1)
            else:
                (x1,y1) = v.computeTransform(x[-1], y[-1])
                x.append(x1)
                y.append(y1)
                
            #Subdivided track between the previous and the next element
            # (the endpoints are already solved for)
            if not subdivide is None:
                if v.canSubdivide == True and (v.length > subdivide):
                    #Find new z positions evenly distributed through the element,
                    # with maximum distance `subdivide`
                
                    nSubdivs = int(v.length / subdivide)
                    for i in range(nSubdivs-1):
                        z = v.length*(i+1)/nSubdivs
                        sSub.append( z + s[-2] )
                        if y0 is None:
                            x1 = v.computeSubdividedTransform(z, x[-2])
                            xSub.append(x1)
                        else:
                            (x1,y1) = v.computeSubdividedTransform(z, x[-2], y[-2])
                            xSub.append(x1)
                            ySub.append(y1)
 
                #Last point in the element is always taken from the full jump
                sSub.append(s[-1])
                xSub.append(x[-1])
                if not y0 is None:
                    ySub.append(y[-1])
        
        s = np.asarray(s)
        
        x = np.asarray(x).T
        if y0 is not None:
            y = np.asarray(y).T
            if subdivide is None:
                return (s,x,y)
            else:
                sSub = np.asarray(sSub)
                xSub = np.asarray(xSub).T
                ySub = np.asarray(ySub).T
                return (s,x,y, sSub,xSub,ySub)
        if subdivide is None:
            return (s, x)
        else:
            sSub = np.asarray(sSub)
            xSub = np.asarray(xSub).T
            return (s,x, sSub,xSub)
        
    def computeTwiss(self, x0, y0=None,\
                     xTra0=np.asarray([0.0,0.0]),yTra0=np.asarray([0.0,0.0]),\
                     subdivide=None):
        """
        Computes twiss functions and centroid trajectory as a function of s.
        
        Input: 
            * Initial twiss parameters x0 and y0 in x and y
                in the format defined in
                CLEARview.elements.Element.computeTwissTransform()
            * Optionally, the initial x- and y centroid trajectory parameters xTra0 and yTra0
                (same formats as for computeTrajectory); defaults to (0.0, 0.0)
            * Optionally, a `subdivide` parameter [m], which means that elements
                which support subdividing will be automatically divided into slices
                no thicker than this value.
                If `subdivide` is given, subdivided versions of s,
                twiss parameters, and trajectories are returned at the end
                of the return value list.
        
        Returns:
            * s    : Array of the non-sliced s [m],
                     i.e. position of the element boundaries,
                     starting at s = 0.0
            * x    : Twiss parameters in X at the given s positions,
                     same format as for x0
            * y    : Same as x but in Y, if y0 is set
            * xTra : Centroid trajectory, same format as x0 and y0
            * yTra : Same as xTra but in Y
            
            Only when subdivide is set:
            * sSub: All the points where the twiss parameters have been computed
            * xSub, ySub, xTraSub, yTraSub: Same as x,y,xTra,yTra, but finer subdivisions.
        """
        
        if not (self.implicitElems_elem is None):
            raise ImplicitNotFinalizedError

        s = []
        x = []
        y = None
        
        xTra = None
        yTra = None
        
        s.append(0.0)
        x.append(x0)
        
        if y0 is not None:
            y = []
            y.append(y0)
            
        #For slicing in case subdivide is switched on
        sSub = None
        xSub = None
        ySub = None
        sTraSub = None #initialized by computeTrajectory()
        xTraSub = None #initialized by computeTrajectory()
        yTraSub = None #initialized by computeTrajectory()
        if not subdivide is None:
            assert type(subdivide) == float
            assert subdivide > 0
            
            sSub = []
            sSub.append(0.0)
            xSub = []
            xSub.append(x0)
            if not y0 is None:
                ySub = []
                ySub.append(y0)

        #Compute the central trajectory, including feed-down etc.
        if y0 is None:
            if subdivide is None:
                (sTra, xTra) = self.computeTrajectory(xTra0)
            else:
                (sTra, xTra, sTraSub,xTraSub) = \
                    self.computeTrajectory(xTra0,subdivide=subdivide)
        else:
            if subdivide is None:
                (sTra, xTra,yTra) = self.computeTrajectory(xTra0,yTra0)
            else:
                (sTra, xTra,yTra, sTraSub,xTraSub,yTraSub) = \
                    self.computeTrajectory(xTra0,yTra0,subdivide=subdivide)

        #Compute TWISS
        for k,v in self.elements.items():
            #Element edge twissfunctions
            s.append(v.length+s[-1])
            if y0 is None:
                x1 = v.computeTwissTransform(x[-1])
                x.append(x1)
            else:
                (x1,y1) = v.computeTwissTransform(x[-1], y[-1])
                x.append(x1)
                y.append(y1)

            #Subdivided twiss functions between the previous- and the next element
            # (the endpoints are already solved, this is really only for plotting prettyness)
            if not subdivide is None:
                if v.canSubdivide == True and (v.length > subdivide):
                    #Find new z positions evenly distributed through the element,
                    # with maximum distance `subdivide`
                
                    nSubdivs = int(v.length / subdivide)
                    for i in range(nSubdivs-1):
                        z = v.length*(i+1)/nSubdivs
                        sSub.append( z + s[-2] )
                        if y0 is None:
                            x1 = v.computeSubdividedTwissTransform(z, x[-2])
                            xSub.append(x1)
                        else:
                            (x1,y1) = v.computeSubdividedTwissTransform(z, x[-2], y[-2])
                            xSub.append(x1)
                            ySub.append(y1)
                # Last point in the element always taken from the full jump
                sSub.append(s[-1])
                xSub.append(x[-1])
                if not y0 is None:
                    ySub.append(y[-1])
        
        s = np.asarray(s)
        
        assert np.all(sTra == s)
        if not subdivide is None:
            assert np.all(sSub == sTraSub)
        
        x = np.asarray(x).T
        if y0 is not None:
            y = np.asarray(y).T
            if subdivide is None:
                return (s,x,y, xTra, yTra)
            else:
                sSub = np.asarray(sSub)
                xSub = np.asarray(xSub).T
                ySub = np.asarray(ySub).T
                return (s,x,y, xTra, yTra, sSub, xSub,ySub, xTraSub,yTraSub)
        if subdivide is None:
            return (s, x, xTra)
        else:
            sSub = np.asarray(sSub)
            xSub = np.asarray(xSub).T
            return (s, x, xTra, sSub,xSub,xTraSub)

    def getTwissMatrix(self, firstElem=None, lastElem=None, lastElemExclusive=False):
        """
        Get the matrix which transforms the Twiss parameters (beta, alpha, gamma)^T
        from the element named firstElem (default: beginning of the line)
        to the element named lastElem (default is in inclusive, e.g. after the element) (default: end of the line).
        Two matrices are returned, one for X and one for Y.
        """

        #Sanity check
        for el in [firstElem, lastElem]:
            if not el is None:
                if not type(el) == str:
                    raise TypeError("firstElem and lastElem should be a str")
                if not el in self.elements.keys():
                    raise KeyError("first/last element '"+el+"' not found in the list of element names.")

        XM = np.eye(3)
        YM = np.eye(3)

        hasStarted = False
        if firstElem is None:
            hasStarted = True

        for k,v in self.elements.items():
            if not hasStarted:
                if k == firstElem:
                    hasStarted = True
                else:
                    #TODO add sanity check, verifying that first is before last
                    continue
            
            if hasStarted and k == lastElem and lastElemExclusive:
                break

            xm_,ym_ = v.getTwissTransform()
            XM = np.matmul(xm_,XM)
            YM = np.matmul(ym_,YM)

            if hasStarted and k == lastElem:
                break

        return (XM,YM)

    def findTwissExtrema(self,x0,y0=None,
                         subdivide=1e-3):
        """
        Finds beta minima and maxima, as well as alpha zero-crossings
        For now it's just a simple search using the given intervals; could be made smarter.
        Input is the same as computeTwiss()
        """

        assert type(subdivide) == float

        def findBetaMin(sSub,xSub):
            sMin    = []
            betaMin = []
            #Remove duplicate S entries
            keepers = np.diff(sSub) > 0.0
            keepers = np.insert(keepers,0,True) #Always Keep the first entry
            sSub_ = sSub[keepers]
            beta_ = xSub[0,keepers]
            
            #Find the local minima
            for i in range(1,len(sSub_)-1):
                if beta_[i] < beta_[i-1] and beta_[i] < beta_[i+1]:
                    sMin    = sSub_[i]
                    betaMin = beta_[i]
            return (sMin,betaMin)

        if not y0 is None:
            (s, x,y, xTra, yTra, sSub,xSub,ySub, xTraSub,yTraSub) = \
                self.computeTwiss(x0,y0, subdivide=subdivide)
            (sMinX, betaMinX) = findBetaMin(sSub,xSub)
            (sMinY, betaMinY) = findBetaMin(sSub,ySub)
            return(sMinX, betaMinX, sMinY, betaMinY)
        else:
            (s, x,   xTra,       sSub,xSub,      xTraSub        ) = \
                self.computeTwiss(x0,    subdivide=subdivide)
            (sMinX, betaMinX) = findBetaMin(sSub,xSub)
            return(sMinX,betaMinX)

## EXCEPTIONS ##
class ElementOverwriteError(Exception):
    pass

class NegativeDriftError(Exception):
    pass

class ImplicitNotFinalizedError(Exception):
    pass
class ImplicitAlreadyFinalizedError(Exception):
    pass
