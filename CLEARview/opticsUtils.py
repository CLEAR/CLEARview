# -*- coding: utf-8 -*-

"""
This file holds various utility functions which are useful throughout the code
and possibly in other codes as well.
"""

import numpy as np

SI_c = 299792458    # [m/s]
mu0  = 4*np.pi*1e-7 # [Tm/A]

def twissTransformMatrix(tm):
    """
    Convert a transfer matrix to a twiss transforming matrix.
    The twiss vector that multiplies the twiss transforming matrix
    should be on the form (beta [m], alpha, gamma)
    """
    
    # S.Y.Lee / Accelerator Physics (3rd ed.), equation 2.36
    return np.asarray([\
                       [tm[0,0]**2      , -2.0*tm[0,0]*tm[0,1]           , tm[0,1]**2      ],\
                       [-tm[0,0]*tm[1,0], tm[0,0]*tm[1,1]+tm[0,1]*tm[1,0], -tm[0,1]*tm[1,1]],\
                       [tm[1,0]**2      , -2.0*tm[1,0]*tm[1,1]           , tm[1,1]**2]\
                       ])

def getGamma(alpha,beta):
    "Compute the gamma [1/m] parameter for the sigma matrix given alpha [-] and beta [m]"
    return(1+alpha**2)/beta

def get_eps_g(eps_n,E0, m0=511e-3):
    """
    Compute the geometrical emittance [um] given the normalized emittance [um]
    and reference energy [MeV].
    By default the particle mass is assumed to be 511keV,
    however this can be overridden by the m0 [MeV/c^2] parameter
    """
    gamma_rel = E0/m0
    #P0 = np.sqrt((E0-m0)*(E0+m0))
    beta_rel = np.sqrt((1.0-1.0/np.sqrt(gamma_rel))*(1.0+1.0/np.sqrt(gamma_rel)))

    return eps_n/(beta_rel*gamma_rel) #[um]

def get_eps_n(eps_g, E0, m0=511e-3):
    "Inverse of get_eps_n"
    gamma_rel = E0/m0
    beta_rel = np.sqrt((1.0-1.0/np.sqrt(gamma_rel))*(1.0+1.0/np.sqrt(gamma_rel)))
    
    return eps_g*beta_rel*gamma_rel

def makeDist(eps_n, beta, alpha, N, E0, m0=511e-3):
    """
    Generate a 1D beam distribution given the optics parameters
    eps_n [um], beta[m], alpha, as well as E0 [MeV] and optionally m0 [MeV/c**2].
    """
    eps_g = get_eps_g(eps_n,E0,m0)
    covar = np.asarray([[beta  , -alpha              ],\
                        [-alpha, getGamma(alpha,beta)]])
    covar *= eps_g*1e-6

    return np.random.multivariate_normal([0.0,0.0],covar,N).T

def getTwiss(x):
    """
    Compute the twiss parameters of a distribution x,
    where x is a vector similar to the one produced by makeDist()
    """
    
    cov = np.cov(x)
    eps_g = np.sqrt(np.linalg.det(cov))
    sigma = cov/eps_g
    beta  = sigma[0,0]
    alpha = -sigma[0,1]
    gamma = sigma[1,1]
        
    return(eps_g, alpha, beta, gamma)
    
def printTwissDist(x, E0, m0=511e-3):
    """
    Compute the twiss parameters of a distribution x,
    and pretty-print the results.
    """
    (eps_g, alpha, beta, gamma) = getTwiss(x)
    
    eps_n = get_eps_n(eps_g,E0,m0)
    
    print("**TWISS FROM DIST**")
    print("\t N         =",x.shape[1])
    print("\t eps_g     =", eps_g*1e6, "[um]")
    print("\t eps_n     =", eps_n*1e6, "[um]")
    print("\t beta      =", beta,  "[m]")
    print("\t alpha     =", alpha)
    print("\t gamma     =", gamma, "[1/m]")
    print("\t gamma_alt =", getGamma(alpha,beta), "[1/m]")

def printLatticeOptics(lattice, s, xt, yt, epsx_g, epsy_g, xTra, yTra):
    # Let's also print the twiss parameters and the sigmas
    sigx = np.sqrt(xt[0,:]*epsx_g*1e-6)*1e3 #[mm]
    sigy = np.sqrt(yt[0,:]*epsy_g*1e-6)*1e3 #[mm]

    print('#  s[m] element_name         typeName            betax[m] betay[m]         alphax[m]        alphay[m] sigmax[mm] sigmay[mm]    x[mm]    y[mm]    dx/ds[1e-3]    dy/ds[1e-3]')
    print('###########################################################################################################################################################################')

    i=0
    print (f'{0.0:7.5} {"START":20} {"-----":20} {xt[0,i]:7.5}  {yt[0,i]:7.5}   {xt[1,i]:15.5}  {yt[1,i]:15.5}    {sigx[i]:7.5}    {sigy[i]:7.5} {xTra[0,i]*1e3:8.5} {yTra[0,i]*1e3:8.5}       {xTra[1,i]*1e3:8.5}       {yTra[1,i]*1e3:8.5}')

    for i,ek in zip(range(1,len(s)),lattice.elements.keys()):
        print (f'{s[i]:7.5} {ek:20} {lattice.elements[ek].typeName:20} {xt[0,i]:7.5}  {yt[0,i]:7.5}   {xt[1,i]:15.5}  {yt[1,i]:15.5}    {sigx[i]:7.5}    {sigy[i]:7.5} {xTra[0,i]*1e3:8.5} {yTra[0,i]*1e3:8.5}       {xTra[1,i]*1e3:8.5}       {yTra[1,i]*1e3:8.5}')


