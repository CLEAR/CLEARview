# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import matplotlib

import CLEARview.elements

###
### General utility functions
###

def vlines(s,ax=None):
    if ax is None:
        ax = plt.gca()
    "Add vertical lines to plot at all element splits"
    for S in s:
        plt.axvline(S,ls='--',color='k')

def addElemsToPlot(ax, lattice):
    "Add background colors to plots corresponding to what they are"
    s=0
    for elemName, elem in lattice.elements.items():
        if isinstance(elem,CLEARview.elements.QuadThick):
            ax.axvspan(s,s+elem.length,alpha=0.3,color='red', zorder=-5)
        elif isinstance(elem, CLEARview.elements.KickerThick):
            ax.axvspan(s,s+elem.length,alpha=0.3,color='blue',zorder=-5)
        elif isinstance(elem, CLEARview.elements.Marker):
            assert elem.length == 0.0
            ax.axvline(s, color='k', ls="--", zorder=-5, linewidth=0.5)
        s = s+elem.length

def addElemNamesToPlot(ax,lattice,elemTypesToSkip=(CLEARview.elements.Drift,), ypos=0.9):
    "Write the names of the elements into the plot"
    
    # the x coords of this transformation are data, and the
    # y coord are axes. From:
    # https://matplotlib.org/users/transforms_tutorial.html
    trans = matplotlib.transforms.\
        blended_transform_factory(ax.transData,ax.transAxes)
    
    sEnd = 0
    for elemName, elem in lattice.elements.items():
        sEnd = sEnd+elem.length
        #Skip unwanted element types
        printName = True
        for elemSkipType in elemTypesToSkip:
            if isinstance(elem,elemSkipType):
                printName = False
        if printName == False:
            continue
        
        ax.text(sEnd-elem.length/2.0, ypos, elemName,\
                transform=trans, \
                horizontalalignment='center', \
                rotation='vertical')
            
###
### Twiss function plotting
###

def alphaBeta(s,xt,yt=None,sSub=None):
    "Plot \alpha and \beta in a new figure"

    if sSub is None:
        sSub = s

    plt.figure()
    
    ax_beta = plt.subplot(2,1,1)
    beta(sSub,xt,yt,ax_beta)
    vlines(s,ax_beta)

    
    plt.legend(loc=0)
    plt.setp(plt.gca().get_xticklabels(),visible=False)

    ax_alpha = plt.subplot(2,1,2, sharex=plt.gca())
    alpha(sSub,xt,yt,ax_alpha)
    vlines(s,ax_alpha)
    
    plt.subplots_adjust(hspace=0.08)
    plt.xlabel('s [m]')
    
    return(ax_beta,ax_alpha)

def alpha(s,xt,yt, ax):
    "Plot \alpha in a given axis"
    ax.set_ylabel(r'$\alpha$')
    ax.plot(s,xt[1,:],label='x')
    if not yt is None:
        ax.plot(s,yt[1,:],label='y')
    ax.axhline(0.0, ls='--',color='k')
    
def beta(s,xt,yt, ax):
    "Plot \beta in a given axis"
    ax.set_ylabel(r'$\beta$ [m]')
    ax.plot(s,xt[0,:],label='x')
    if not yt is None:
        ax.plot(s,yt[0,:],label='y')

###
### Sigma envelope plotting
###

def sigmas(s,xt,yt, eps_g, xTra,yTra, nsig=(1,2,3),sSub=None, equalY=False):
    "Plot sigma_x and sigma_y to a new figure"
    
    if sSub is None:
        sSub = s

    if np.isscalar(eps_g):
        epsx_g = eps_g
        epsy_g = eps_g
    else:
        if len(eps_g) == 2:
            epsx_g = eps_g[0]
            epsy_g = eps_g[1]
        else:
             raise ValueError('Expected 1 or 2 emittances')

    plt.figure()

    ax1 = plt.subplot(2,1,1)
    sigma_singleplane(sSub,xt,xTra, nsig,epsx_g,'blue',ax1,'x')
    vlines(s)
    plt.setp(plt.gca().get_xticklabels(),visible=False)

    ax2=plt.subplot(2,1,2, sharex=plt.gca())
    sigma_singleplane(sSub,yt,yTra,nsig,epsy_g,'red',ax2,'y')
    vlines(s)
    
    plt.xlabel('s [m]')
    plt.subplots_adjust(hspace=0.0)
    
    if equalY:
        (xmin,xmax) = ax1.get_ylim()
        (ymin,ymax) = ax2.get_ylim()
        maxmax = np.max(np.abs([xmin,xmax,ymin,ymax]))
        ax1.set_ylim(-maxmax,maxmax)
        ax2.set_ylim(-maxmax,maxmax)

    return (ax1,ax2)

def sigma_singleplane(s, xt,xTra, nsig,eps_g,color, ax, ylab='x',alph0=1.0):
    "Plot one sigma in a given axis"
    sig = np.sqrt(xt[0,:]*eps_g*1e-6)

    plots = []
    
    for ns in nsig:
        alph = alph0/2.0**(ns-1)
        plots.append( ax.fill_between(s, (ns*sig+xTra[0,:])*1e3, (-ns*sig+xTra[0,:])*1e3, alpha=alph, color=color) )
    ax.set_ylabel(ylab+' [mm]')
    ax.axhline(0.0, ls='--',color='k')

    return plots

###
### Particle tracks plotting
###
    
def tracks(s,xDist,yDist, sSub=None):
    "Plot the tracks in both planes in a new figure"
    
    if sSub is None:
        sSub=s
    plt.figure()
    ax1 = plt.subplot(2,1, 1)
    tracks_singleplane(sSub,xDist,ax1,'x')
    vlines(s)
    plt.setp(plt.gca().get_xticklabels(),visible=False)
    
    ax2=plt.subplot(2,1, 2, sharex=plt.gca())
    tracks_singleplane(sSub,yDist,ax2,'y')
    vlines(s)

    plt.xlabel('s [m]')
    plt.subplots_adjust(hspace=0.0)
    
    return(ax1,ax2)

def tracks_singleplane(s,xDist,ax,ylab='x'):
    if len(xDist.shape) == 1:
        ax.plot(s,xDist[0,:], label=ylab)
    else:
        Npart = xDist.shape[0]
    
        for pIdx in range(Npart):
            ax.plot(s,xDist[pIdx,0,:]*1e3)
    
    plt.axhline(0.0, ls='--',color='k')
    plt.ylabel(ylab+' [mm]')

