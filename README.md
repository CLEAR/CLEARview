# CLEARview README

This repository contains CLEARview, which is:
* A Python linear optics library which can do 4D and 2D tracking and Twiss
* An online optics estimation GUI used for CLEAR operation
* A lattice file converter from MAD-X survey files
* Various plotting utilities
* A collection of elements (Drift, thin/thick kicker, thin/thick quadrupole, plasma lens, specialized CLEAR quadrupoles and kicker definitions)

CLEARview is written by Kyrre Sjobak.
