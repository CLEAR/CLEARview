#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

#Assumes that we are starting it from the directory of 'onlineGUI.py'
sys.path.append("../.")
import CLEARview
import onlineModel

import PyQt5.QtWidgets
import PyQt5.QtGui
import PyQt5.QtCore

import matplotlib
import matplotlib.backends.backend_qt5agg
import matplotlib.pyplot as plt

import threading

import numpy as np

class CLEARview_online(PyQt5.QtWidgets.QMainWindow):
    model = None
    
    timer  = None
    update = 0 # How often to auto-update the plot [s]; 0 = Don't
    update_lock = None
    
    #Note: The widgets are pushed to `self` on the fly in __init__
    
    def __init__(self):
        PyQt5.QtWidgets.QMainWindow.__init__(self)
        
        #Ask the user to pick the model to use
        items = ("CLEAR (online)","CLEAR (offline/testing)", "Old CLEAR (online)", "Old CLEAR (offline/testing)")
        item, okPressed = PyQt5.QtWidgets.QInputDialog.getItem(self,"Online optics model","Select model:", items, 0, False)
        if okPressed and item:
            if item == "CLEAR (online)":
                self.model = onlineModel.CLEARonline_autoconverted_online()
            elif item == "CLEAR (offline/testing)":
                self.model = onlineModel.CLEARonline_autoconverted_dummy()
            elif item == "Old CLEAR (online)":
                self.model = onlineModel.CLEARonline_online()
            elif item == "Old CLEAR (offline/testing)":
                self.model = onlineModel.CLEARonline_dummy()
            else:
                print(f"Internal error in model selection, got model='{item}'")
                exit(-1)

        else:
            print()
            print("Model selection was canceled")
            print()
            
            exit(-1)

        #Initialize the model
        self.model.update()
        self.model.recalculate()

        #Initialize the timer
        self.timer = PyQt5.QtCore.QTimer()
        self.timer.timeout.connect(self.do_update)
        self.update_lock = threading.Lock()

        ## Start setting up the window ##

        self.setAttribute(PyQt5.QtCore.Qt.WA_DeleteOnClose)

        # Use a fixed window title, that makes CCM happy.
        #self.setWindowTitle("Online optics model for {}".format(item))
        self.setWindowTitle("Online optics model")
        
        #Populate the window!
        mainWidget = PyQt5.QtWidgets.QWidget(self) 
        self.setCentralWidget(mainWidget)
        vbox = PyQt5.QtWidgets.QVBoxLayout(mainWidget)
        #self.setLayout(vbox)
        
        #Put a plot on top
        self.sigmaCanvas = DoubleSigmaCanvas(self.model,mainWidget, width=10,height=4)
        vbox.addWidget(self.sigmaCanvas)
        #Navigation
        self.plotNavBar = matplotlib.backends.backend_qt5agg.NavigationToolbar2QT(self.sigmaCanvas,mainWidget)
        vbox.addWidget(self.plotNavBar)

        #Extra plot controls
        hbox = PyQt5.QtWidgets.QHBoxLayout(mainWidget)
        vbox.addLayout(hbox)

        self.button_equalizeY = PyQt5.QtWidgets.QPushButton("Equalize plots", self)
        self.button_equalizeY.clicked.connect(self.click_equalizeY)
        hbox.addWidget(self.button_equalizeY)

        self.button_tophalf = PyQt5.QtWidgets.QPushButton("Show only positive", self)
        self.button_tophalf.clicked.connect(self.click_tophalf)
        hbox.addWidget(self.button_tophalf)

        # Divider
        hline1 = PyQt5.QtWidgets.QFrame()
        hline1.setFrameShape(PyQt5.QtWidgets.QFrame.HLine)
        vbox.addWidget(hline1)

        # Grid down below
        grid = PyQt5.QtWidgets.QGridLayout()
        vbox.addLayout(grid)
        ##Fill the grid!
        # Greek letters copy-pasted from
        # https://en.wikipedia.org/wiki/Mathematical_operators_and_symbols_in_Unicode#Mathematical_Alphanumeric_Symbols_block
        # Subset = "MATHEMATICAL BOLD ITALIC SMALL *"
        
        ## X parameters
        grid.addWidget( PyQt5.QtWidgets.QLabel("<b>X parameters</b>, σ [mm] ="), 0,0)
        self.txt_sigmaX = PyQt5.QtWidgets.QLineEdit(self)
        self.txt_sigmaX.setDisabled(True)
        grid.addWidget( self.txt_sigmaX, 0,1)
        
        grid.addWidget( PyQt5.QtWidgets.QLabel("𝜺<sub>x</sub> [µm] = "), 1,0 )
        self.txt_epsNx = PyQt5.QtWidgets.QLineEdit(self)
        self.txt_epsNx.setText(str(self.model.epsN_x))
        self.txt_epsNx.editingFinished.connect(self.edit_epsNx)
        self.txt_epsNx.setValidator(PyQt5.QtGui.QDoubleValidator())
        grid.addWidget( self.txt_epsNx, 1,1)
        
        grid.addWidget( PyQt5.QtWidgets.QLabel("𝜷<sub>x</sub> [m] = "), 0,2 )
        self.txt_beta0_x = PyQt5.QtWidgets.QLineEdit(self)
        self.txt_beta0_x.setText(str(self.model.beta0_x))
        self.txt_beta0_x.editingFinished.connect(self.edit_beta0_x)
        self.txt_beta0_x.setValidator(PyQt5.QtGui.QDoubleValidator())
        grid.addWidget( self.txt_beta0_x, 0,3)
        
        grid.addWidget( PyQt5.QtWidgets.QLabel("𝜶<sub>x</sub> = "), 1,2 )
        self.txt_alpha0_x = PyQt5.QtWidgets.QLineEdit(self)
        self.txt_alpha0_x.setText(str(self.model.alpha0_x))
        self.txt_alpha0_x.editingFinished.connect(self.edit_alpha0_x)
        self.txt_alpha0_x.setValidator(PyQt5.QtGui.QDoubleValidator())
        grid.addWidget( self.txt_alpha0_x, 1,3)

        grid.addWidget( PyQt5.QtWidgets.QLabel("x<sub>0</sub> [mm] = "), 2,0 )
        self.txt_x0 = PyQt5.QtWidgets.QLineEdit(self)
        self.txt_x0.setText(str(self.model.x0))
        self.txt_x0.editingFinished.connect(self.edit_x0)
        self.txt_x0.setValidator(PyQt5.QtGui.QDoubleValidator())
        grid.addWidget( self.txt_x0, 2,1)
        
        grid.addWidget( PyQt5.QtWidgets.QLabel("x'<sub>0</sub> [10<sup>-3</sup>]= "), 2,2 )
        self.txt_xp0 = PyQt5.QtWidgets.QLineEdit(self)
        self.txt_xp0.setText(str(self.model.xp0))
        self.txt_xp0.editingFinished.connect(self.edit_xp0)
        self.txt_xp0.setValidator(PyQt5.QtGui.QDoubleValidator())
        grid.addWidget( self.txt_xp0, 2,3)
        
        ## DIVIDER
        vline1 = PyQt5.QtWidgets.QFrame()
        vline1.setFrameShape(PyQt5.QtWidgets.QFrame.VLine)
        grid.addWidget(vline1,0,4,3,1)
        
        ## Y parameters
        grid.addWidget( PyQt5.QtWidgets.QLabel("<b>Y parameters:</b>, σ [mm] ="), 0,5)
        self.txt_sigmaY = PyQt5.QtWidgets.QLineEdit(self)
        self.txt_sigmaY.setDisabled(True)
        grid.addWidget( self.txt_sigmaY, 0,6)

        grid.addWidget( PyQt5.QtWidgets.QLabel("𝜺<sub>y</sub> [µm] = "), 1,5)
        self.txt_epsNy = PyQt5.QtWidgets.QLineEdit(self)
        self.txt_epsNy.setText(str(self.model.epsN_y))
        self.txt_epsNy.editingFinished.connect(self.edit_epsNy)
        self.txt_epsNy.setValidator(PyQt5.QtGui.QDoubleValidator())
        grid.addWidget( self.txt_epsNy, 1,6)

        grid.addWidget( PyQt5.QtWidgets.QLabel("𝜷<sub>y</sub> [m] = "), 0,7 )
        self.txt_beta0_y = PyQt5.QtWidgets.QLineEdit(self)
        self.txt_beta0_y.setText(str(self.model.beta0_y))
        self.txt_beta0_y.editingFinished.connect(self.edit_beta0_y)
        self.txt_beta0_y.setValidator(PyQt5.QtGui.QDoubleValidator())
        grid.addWidget( self.txt_beta0_y, 0,8)
        
        grid.addWidget( PyQt5.QtWidgets.QLabel("𝜶<sub>y</sub> = "), 1,7 )
        self.txt_alpha0_y = PyQt5.QtWidgets.QLineEdit(self)
        self.txt_alpha0_y.setText(str(self.model.alpha0_y))
        self.txt_alpha0_y.editingFinished.connect(self.edit_alpha0_y)
        self.txt_alpha0_y.setValidator(PyQt5.QtGui.QDoubleValidator())
        grid.addWidget( self.txt_alpha0_y, 1,8)

        grid.addWidget( PyQt5.QtWidgets.QLabel("y<sub>0</sub> [mm] = "), 2,5 )
        self.txt_y0 = PyQt5.QtWidgets.QLineEdit(self)
        self.txt_y0.setText(str(self.model.x0))
        self.txt_y0.editingFinished.connect(self.edit_y0)
        self.txt_y0.setValidator(PyQt5.QtGui.QDoubleValidator())
        grid.addWidget( self.txt_y0, 2,6)

        grid.addWidget( PyQt5.QtWidgets.QLabel("y'<sub>0</sub> [10<sup>-3</sup>]= "), 2,7 )
        self.txt_yp0 = PyQt5.QtWidgets.QLineEdit(self)
        self.txt_yp0.setText(str(self.model.yp0))
        self.txt_yp0.editingFinished.connect(self.edit_yp0)
        self.txt_yp0.setValidator(PyQt5.QtGui.QDoubleValidator())
        grid.addWidget( self.txt_yp0, 2,8)
        
        ## DIVIDER
        vline2 = PyQt5.QtWidgets.QFrame()
        vline2.setFrameShape(PyQt5.QtWidgets.QFrame.VLine)
        grid.addWidget(vline2,0,9,3,1)

        ## General parameters        
        grid.addWidget( PyQt5.QtWidgets.QLabel("P<sub>0</sub> [MeV/c] = "), 0,10 )
        self.txt_P0 = PyQt5.QtWidgets.QLineEdit(self)
        self.txt_P0.setText(str(self.model.P0))
        self.txt_P0.editingFinished.connect(self.edit_P0)
        self.txt_P0.setValidator(PyQt5.QtGui.QDoubleValidator())
        grid.addWidget( self.txt_P0, 0,11)

        grid.addWidget( PyQt5.QtWidgets.QLabel("Update every [s] = "), 1,10 )
        self.txt_update = PyQt5.QtWidgets.QLineEdit(self)
        self.txt_update.setText(str(self.update))
        self.txt_update.editingFinished.connect(self.edit_update)
        self.txt_update_validator = PyQt5.QtGui.QIntValidator()
        self.txt_update_validator.setBottom(0)
        self.txt_update.setValidator(self.txt_update_validator)
        grid.addWidget( self.txt_update, 1,11)

        ## Some buttons
        self.button_update = PyQt5.QtWidgets.QPushButton('Update now',self)
        self.button_update.setSizePolicy(\
                                         PyQt5.QtWidgets.QSizePolicy.Preferred,
                                         PyQt5.QtWidgets.QSizePolicy.Expanding)
        self.button_update.clicked.connect(self.click_update)
        grid.addWidget(self.button_update, 0,12,1,1)
        
        self.checkbox_ignoreCorrectors = PyQt5.QtWidgets.QCheckBox("Ignore correctors")
        if self.model.ignoreCorrectors == True:
            self.checkbox_ignoreCorrectors.setCheckState(PyQt5.QtCore.Qt.Checked)
        else:
            self.checkbox_ignoreCorrectors.setCheckState(PyQt5.QtCore.Qt.Unchecked)
        self.checkbox_ignoreCorrectors.stateChanged.connect(self.change_ignoreCorrectors)
        grid.addWidget(self.checkbox_ignoreCorrectors, 1,12,1,1)
        
        ## Starting point (defined after checkbox_ignoreCorrectors as it accesses it)
        grid.addWidget( PyQt5.QtWidgets.QLabel("Starting point:"), 2,10 )
        self.combo_initial = PyQt5.QtWidgets.QComboBox()
        self.combo_initial.currentIndexChanged.connect(self.change_startingPoint)
        for s in self.model.startingPoints_possible:
            self.combo_initial.addItem(s)
        grid.addWidget(self.combo_initial, 2,11,1,2)

        ## DIVIDER
        vline3 = PyQt5.QtWidgets.QFrame()
        vline3.setFrameShape(PyQt5.QtWidgets.QFrame.VLine)
        grid.addWidget(vline3,0,13,3,1)

        ## HELP button
        self.button_help = PyQt5.QtWidgets.QPushButton('H\nE\nL\nP',self)
        self.button_help.setSizePolicy(\
                                       PyQt5.QtWidgets.QSizePolicy.Minimum,
                                       PyQt5.QtWidgets.QSizePolicy.Expanding)
        self.button_help.clicked.connect(self.click_help)
        grid.addWidget(self.button_help, 0,14,3,1)
        
        self.show()
        
    #Functions for applying the updated values
    def edit_epsNx(self):
        self.model.epsN_x = float( self.txt_epsNx.text() )
        self.model.needRecalculate = True
        self.do_update()
    def edit_epsNy(self):
        self.model.epsN_y = float( self.txt_epsNy.text() )
        self.model.needRecalculate = True
        self.do_update()
    def edit_beta0_x(self):
        self.model.beta0_x = float( self.txt_beta0_x.text() )
        self.model.needRecalculate = True
        self.do_update()
    def edit_beta0_y(self):
        self.model.beta0_y = float( self.txt_beta0_y.text() )
        self.model.needRecalculate = True
        self.do_update()
    def edit_alpha0_x(self):
        self.model.alpha0_x = float( self.txt_alpha0_x.text() )
        self.model.needRecalculate = True
        self.do_update()
    def edit_alpha0_y(self):
        self.model.alpha0_y = float( self.txt_alpha0_y.text() )
        self.model.needRecalculate = True
        self.do_update()
    def edit_x0(self):
        self.model.x0 = float( self.txt_x0.text() )
        self.model.needRecalculate = True
        self.do_update()
    def edit_y0(self):
        self.model.y0 = float( self.txt_y0.text() )
        self.model.needRecalculate = True
        self.do_update()
    def edit_xp0(self):
        self.model.xp0 = float( self.txt_xp0.text() )
        self.model.needRecalculate = True
        self.do_update()
    def edit_yp0(self):
        self.model.yp0 = float( self.txt_yp0.text() )
        self.model.needRecalculate = True
        self.do_update()
    def edit_P0(self):
        self.model.P0 = float( self.txt_P0.text() )
        self.model.needRecalculate = True
        self.do_update()
    
    def change_ignoreCorrectors(self, state):
        if state == PyQt5.QtCore.Qt.Checked:
            self.model.ignoreCorrectors = True
        else:
            self.model.ignoreCorrectors = False
        self.do_update()
    
    def change_startingPoint(self, idx):
        self.model.startingPoint_set(idx)
        
        # Update the textboxes
        self.txt_beta0_x.setText( str(self.model.beta0_x) )
        self.txt_beta0_x.setCursorPosition(0)
        self.txt_beta0_y.setText( str(self.model.beta0_y) )
        self.txt_beta0_y.setCursorPosition(0)
        self.txt_alpha0_x.setText( str(self.model.alpha0_x) )
        self.txt_alpha0_x.setCursorPosition(0)
        self.txt_alpha0_y.setText( str(self.model.alpha0_y) )
        self.txt_alpha0_y.setCursorPosition(0)

        self.do_update_txtSigma()

        #self.txt_x0.setText( str(self.model.x0) )
        #self.txt_x0.setCursorPosition(0)
        #self.txt_y0.setText( str(self.model.y0) )
        #self.txt_y0.setCursorPosition(0)
        #self.txt_xp0.setText( str(self.model.xp0) )
        #self.txt_xp0.setCursorPosition(0)
        #self.txt_yp0.setText( str(self.model.yp0) )
        #self.txt_yp0.setCursorPosition(0)

        #TODO implement backpropagation of trajectory
        if idx != 0:
            if not self.checkbox_ignoreCorrectors.checkState():
                self.checkbox_ignoreCorrectors.setCheckState(PyQt5.QtCore.Qt.Checked)
                self.do_update()
            self.checkbox_ignoreCorrectors.setDisabled(True)
        else:
            self.checkbox_ignoreCorrectors.setDisabled(False)

    def edit_update(self):
        self.update = int( self.txt_update.text() )
        
        if self.timer.isActive():
            self.timer.stop()
        if self.update > 0:
            self.timer.start(self.update*1000)

    def click_equalizeY(self):
        (xmin,xmax) = self.sigmaCanvas.axes[0].get_ylim()
        (ymin,ymax) = self.sigmaCanvas.axes[1].get_ylim()
        maxmax = np.max(np.abs([xmin,xmax,ymin,ymax]))

        self.sigmaCanvas.axes[0].set_ylim(-maxmax,maxmax)
        self.sigmaCanvas.axes[1].set_ylim(-maxmax,maxmax)
        self.sigmaCanvas.draw()

        return maxmax

    def click_tophalf(self):
        maxmax = self.click_equalizeY()

        self.sigmaCanvas.axes[0].set_ylim(0,maxmax)
        self.sigmaCanvas.axes[1].set_ylim(0,maxmax)
        self.sigmaCanvas.draw()

    def click_update(self):
        self.do_update()
        
    def click_help(self):
        msg = PyQt5.QtWidgets.QMessageBox()
        msg.setWindowTitle("Online optics model HELP")
        msg.setText("Welcome to the CLEARview Online Optics Model!\n"+\
                    "This code is written and maintained by Kyrre Sjobak, k.n.sjobak@fys.uio.no\n"+\
                    "\n"+\
                    "This program estimates the beam envelope based on the Twiss parameters at a given starting point, and the measured magnet currents, using a monochromatic linear optics model."+\
                    "1,2, and 3 sigma lines are plotted for the horizontal- and vertical plane, using different shading in the plot.\n"+\
                    "\n"+\
                    "To use it, first measure the beam momentum and the Twiss parameters with e.g. a quad scan, and then enter the estimated normalized emittance in the textboxes.\n"+\
                    "It then propagates the Twiss parameters from the START to the end of the line and computes the beam sigma from it. "+\
                    "Everything is computed at the edge of every element and at short intervals inside the elements.\n"+\
                    "If a different starting point than START is selected, it will first back-propagate the measured Twiss parameters to the beginning of the line, then forward-propagate as normal from there. "+\
                    "The Twiss parameters at the currently selected starting point is always kept fixed, including if changing upstream magnets. "+\
                    "If changing the starting point dropdown, the Twiss parameters at the other point will be displayed in the textboxes and kept fixed. "+\
                    "It is therefore a good idea to change the starting point to START after having set the Twiss parameters at a measured location, so that this can be taken into account.\n\n"+\
                    "The envelope is recalculated whenever the 'Update now' button is pushed, when a textbox is manually changed, or at a fixed timed interval. If the interval is set to 0, automatic updates are disabled.\n"+\
                    "\n"+\
                    "The model can also estimate the trajectory based on corrector settings, initial beam trajectory parameters, and quadrupole feed-down, however this is not very accurate as the quad offsets and initial beam parameters are not well known.")
        msg.exec()
    
    def do_update(self):
        "Routine to trigger an update of the plot, automatic or not."
        with self.update_lock:
            #print("UPDATE")
            self.model.update()
            self.model.recalculate()
            self.sigmaCanvas.update_figure()
            self.do_update_txtSigma()
            
    
    def do_update_txtSigma(self):
        epsg_x = CLEARview.opticsUtils.get_eps_g(self.model.epsN_x,self.model.P0) #[um]
        self.txt_sigmaX.setText( str( np.sqrt(epsg_x * self.model.beta0_x) ) )    #[mm]
        self.txt_sigmaX.setCursorPosition(0)

        epsg_y= CLEARview.opticsUtils.get_eps_g(self.model.epsN_y,self.model.P0) #[um]
        self.txt_sigmaY.setText( str( np.sqrt(epsg_y * self.model.beta0_y) ) )   #[mm]
        self.txt_sigmaY.setCursorPosition(0)

class MplCanvas(matplotlib.backends.backend_qt5agg.FigureCanvasQTAgg):
    """
    General embeddable matplotlib canvas class.
    Implement `compute_initial_figure()` and `update_figure()` to use it.
    Based on https://matplotlib.org/examples/user_interfaces/embedding_in_qt5.html
    """
    
    axes = None
    
    def __init__(self, parent=None, width=5, height=4, dpi=100, numAxesHor=1):
        self.fig = matplotlib.figure.Figure(figsize=(width, height), dpi=dpi)
        
        
        self.axes = []
        for i in range(numAxesHor):
            if i==0:
                ax = self.fig.add_subplot(numAxesHor,1,i+1)
            else:
                ax = self.fig.add_subplot(numAxesHor,1,i+1,sharex=self.axes[0])
                
            self.axes.append(ax)

        self.compute_initial_figure()

        matplotlib.backends.backend_qt5agg.FigureCanvasQTAgg.__init__(self, self.fig)
        self.setParent(parent)

        matplotlib.backends.backend_qt5agg.FigureCanvasQTAgg.setSizePolicy(self, \
                           PyQt5.QtWidgets.QSizePolicy.Expanding, \
                           PyQt5.QtWidgets.QSizePolicy.Expanding)
        matplotlib.backends.backend_qt5agg.FigureCanvasQTAgg.updateGeometry(self)

    def compute_initial_figure(self):
        #To be implemented in inheriting class
        raise NotImplementedError()
    
    def update_figure(self):
        #To be implemented in inheriting class
        raise NotImplementedError()

    def resizeEvent(self,event):
        super().resizeEvent(event)
        self.fig.tight_layout()
        
class DoubleSigmaCanvas(MplCanvas):
    "Matplotlib canvas which plots the beam sigma envelopes"
    model = None
    
    def __init__(self,model,parent=None, width=5, height=4, dpi=100):
        self.model = model
        
        MplCanvas.__init__(self,parent,width,height,dpi,numAxesHor=2)
        
    def compute_initial_figure(self):
        #TODO: E0/P0
        
        CLEARview.plot.sigma_singleplane(self.model.sSub,\
                                         self.model.xtSub, \
                                         self.model.xTraSub, \
                                         (1,2,3), \
                                         CLEARview.opticsUtils.get_eps_g(self.model.epsN_x,self.model.P0),\
                                         'blue',\
                                         self.axes[0] )
        CLEARview.plot.addElemsToPlot(self.axes[0], self.model.lattice)
        plt.setp(self.axes[0].get_xticklabels(),visible=False)
        self.axes[0].set_ylabel('x [mm]')
        CLEARview.plot.addElemNamesToPlot(self.axes[0],self.model.lattice)

        CLEARview.plot.sigma_singleplane(self.model.sSub,\
                                         self.model.ytSub, \
                                         self.model.yTraSub, \
                                         (1,2,3), \
                                         CLEARview.opticsUtils.get_eps_g(self.model.epsN_y,self.model.P0),\
                                         'red',\
                                         self.axes[1] )
        CLEARview.plot.addElemsToPlot(self.axes[1], self.model.lattice)
        self.axes[1].set_xlabel('s [m]')
        self.axes[1].set_ylabel('y [mm]')
    
    def update_figure(self):
        #Here we assume that the recalculation was already triggered from outside
        xlim1_up,xlim1_dn = self.axes[0].get_xlim()
        xlim2_up,xlim2_dn = self.axes[1].get_xlim()
        ylim1_up,ylim1_dn = self.axes[0].get_ylim()
        ylim2_up,ylim2_dn = self.axes[1].get_ylim()

        #print(xlim1_up, xlim1_dn)

        self.axes[0].cla()
        self.axes[1].cla()
        
        self.compute_initial_figure()

        self.axes[0].set_xlim(xlim1_up,xlim1_dn)
        self.axes[1].set_xlim(xlim2_up,xlim2_dn)
        self.axes[0].set_ylim(ylim1_up,ylim1_dn)
        self.axes[1].set_ylim(ylim2_up,ylim2_dn)

        self.draw()

    def resizeEvent(self,event):
        super().resizeEvent(event)
        self.fig.subplots_adjust(hspace=0.0)

#Kicks off the application
if __name__ == "__main__":
    
    qApp = PyQt5.QtWidgets.QApplication(sys.argv)
    
    #Run!
    aw = CLEARview_online()
    aw.show()
    sys.exit(qApp.exec_())
