#! /usr/bin/env bash
# Little script to start onlineGUI from e.g. CCM

source /acc/local/share/python/acc-py/setup.sh

#From https://stackoverflow.com/questions/4774054/reliable-way-for-a-bash-script-to-get-the-full-path-to-itself
#Same as for BCMgui
SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
onlineGuiPATH=$SCRIPTPATH

echo "cd to "$onlineGuiPATH" ..."
cd $onlineGuiPATH

python3 onlineGUI.py
