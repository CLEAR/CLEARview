# -*- coding: utf-8 -*-


import sys
sys.path.append("../.")
import CLEARview
sys.path.append("../lattices/.")

import numpy as np


class onlineModel:
    "Generic onlineModel class, to be used in onlineGUI.py"
    
    #CLEARview.lattice.Lattice object to use
    lattice = None
    
    subdivide = 0.1 #[m] Maximum length of S before slicing
    
    ## Beam initial parameters, defined at self.startingPoint
    P0 = None #[MeV/c]
    #Twiss functions
    epsN_x = None #[um]
    epsN_y = None #[um]
    beta0_x = None #[m]
    beta0_y = None #[m]
    alpha0_x = None
    alpha0_y = None
    #Trajectory
    x0  = None #[mm]
    xp0 = None #[1e-3]
    y0  = None #[mm]
    yp0 = None #[1e-3]
    
    #Where they are defined from
    startingPoint = 'START'        # Current starting point, 'START' or an element name
    startingPoints_possible = None # List of element names, including START as the first element.
                                   # Element entrance is used in case of thick element
    startingPoint_TwissIdx  = 0    # Current index into xt/xTra/etc.
    startingPoints_TwissIdx = None # List of indices into xt/xTra/etc.
    startingPoints_idx      = None # Their index in the xt/xTra/etc. lists

    ## Output parameters
    needRecalculate = True

    #Non-sliced values
    s = None
    xt = None #beta_x, alpha_x, gamma_x [m,1,1/m]
    yt = None #beta_y, alpha_y, gamma_y [m,1,1/m]
    xTra = None  #x,x' [mm, 1e-3]
    yTra = None  #y,y' [mm, 1e-3]
    #Sliced values, same meaning as non-sliced, just denser
    sSub = None
    xtSub = None
    ytSub = None
    xTraSub = None
    yTraSub = None
    
    #Set this to true if update/recalculate should ignore the correctors
    ignoreCorrectors = True
    
    def __init__(self):
        pass
    
    def update(self):
        "Read the new magnet currents from the machine"
        self.needRecalculate = True
    
    def startingPoint_set(self,idx):
        if self.needRecalculate == True:
            # This will also initialize the startingPoint_* arrays if needed
            self.recalculate()

        if idx >= len(self.startingPoints_possible) or idx < 0:
            raise ValueError('Got idx=',str(idx),', which is too big or too small.')

        else:
            self.startingPoint_TwissIdx = self.startingPoints_TwissIdx[idx]
            self.startingPoint = self.startingPoints_possible[idx]

            print(self.xt[0][0], self.xt[1][0])
            self.beta0_x  = self.xt[0][self.startingPoint_TwissIdx]
            self.beta0_y  = self.yt[0][self.startingPoint_TwissIdx]
            self.alpha0_x = self.xt[1][self.startingPoint_TwissIdx]
            self.alpha0_y = self.yt[1][self.startingPoint_TwissIdx]

            #print(self.x0)
            #self.x0       = self.xTra[self.startingPoint_TwissIdx][0]
            #self.y0       = self.yTra[self.startingPoint_TwissIdx][0]
            #self.xp0      = self.xTraSub[self.startingPoint_TwissIdx][1]
            #self.yp0      = self.yTraSub[self.startingPoint_TwissIdx][1]

    def recalculate(self):
        "Recompute the parameters from the lattice"

        if self.startingPoints_idx == None:
            #Initialization of starting points
            self.startingPoints_TwissIdx  = []

            if not (self.startingPoints_possible[0] == 'START'):
                raise ValueError('startingPoints_possible must begin with START')
            #'START' aka s=0 is returned as the first value by the Twiss calculation
            self.startingPoints_TwissIdx.append(0)

            allPoints = list(self.lattice.elements.keys())
            for startingPoint in self.startingPoints_possible:
                for j in range(len(allPoints)):
                    if startingPoint == allPoints[j]:
                        # There is a shift of +1 between the elements list and
                        # the output of the Twiss calculation, which adds an extra point at s=0.
                        # However we want to get the data at the start of the element, not the end,
                        # so we need to look at the element before -> shift of -1.
                        # These two shifts cancel.
                        self.startingPoints_TwissIdx.append(j)

        if self.needRecalculate == False:
            return
        print('recalculating...')

        xt0 = np.asarray([self.beta0_x,self.alpha0_x,\
                          CLEARview.opticsUtils.getGamma(self.alpha0_x,self.beta0_x)])
        yt0 = np.asarray([self.beta0_y,self.alpha0_y,\
                          CLEARview.opticsUtils.getGamma(self.alpha0_y,self.beta0_y)])

        print(self.startingPoint)
        if self.startingPoint != 'START':
            #Backpropagate optics
            (XM,YM) = self.lattice.getTwissMatrix(firstElem=None,lastElem=self.startingPoint,lastElemExclusive=True)
            
            xt0 = np.linalg.solve(XM,xt0)
            yt0 = np.linalg.solve(YM,yt0)
            
            #TODO: Also backpropagate the trajectory

        xTra0 = np.asarray([self.x0*1e-3,self.xp0*1e-3])
        yTra0 = np.asarray([self.y0*1e-3,self.yp0*1e-3])
    
        assert self.subdivide is not None
        
        (self.s,   self.xt,   self.yt,   self.xTra,   self.yTra, \
         self.sSub,self.xtSub,self.ytSub,self.xTraSub,self.yTraSub) =\
            self.lattice.computeTwiss(xt0,yt0, \
                                      xTra0,yTra0,\
                                      subdivide=self.subdivide)
        
        if self.ignoreCorrectors:
            self.xTra *= 0.0
            self.yTra *= 0.0
            self.xTraSub *= 0.0
            self.yTraSub *= 0.0
        
        self.needRecalculate = False

        #print (self.s, self.xt, self.xTra,self.lattice.elements.keys())
        #print(len(self.s), len(self.lattice.elements.keys()))
        
class CLEARonline(onlineModel):
    "Base class for the _online- and _dummy CLEAR lattice classes"
    
    #Typical beam parameters
    P0 = 200.0 #[MeV/c]
    epsN_x = 10.0 #[um]
    epsN_y = 10.0 #[um]
    beta0_x = 2.0 #[m]
    beta0_y = 2.0 #[m]
    alpha0_x = 0.0
    alpha0_y = 0.0
    
    x0  = 0.0 #[mm]
    y0  = 0.0 #[1e-3]
    xp0 = 0.0 #[mm]
    yp0 = 0.0 #[1e-3]
    
    startingPoints_possible = ['START', 'BTV0390', 'QFD0510', 'BTV0620', 'BTV0730', 'QFD0760', 'BTV0810', 'QDD0870', 'BTV0910']

    def __init__(self):
        onlineModel.__init__(self)
        import CLEAR #get the lattice
        self.lattice=CLEAR.buildCLEAR(self.P0)
        
class CLEARonline_online(CLEARonline):
    japc = None
    
    def __init__(self):
        CLEARonline.__init__(self)
        import pyjapc
        self.japc = pyjapc.PyJapc("SCT.USER.SETUP")
    
    def update(self):
        "Get the current magnet currents from japc"
        super().update()
        
        for name, element in self.lattice.elements.items():
            if type(element) == CLEARview.elements.CLEAR_Q:
                #print ("Q", name)
                I = self.getCur(name)
                if name[1] == "F":
                    #if name == "QFD0520":
                    #    #Hack to compensate for one quadrupole being used
                    #    # with opposite polarity
                    #    element.update(-I,self.P0)
                    #else:
                    element.update(I,self.P0)
                elif name[1] == "D":
                    element.update(-I,self.P0)
                else:
                    print ("Strange quad name '{}'".format(name))
            elif type(element) == CLEARview.elements.CLEAR_DG120:
                #print  ("120", name)
                Ix = self.getCur("DH"+name[1:])
                Iy = self.getCur("DV"+name[1:])
                element.update(Ix,Iy,self.P0)
            elif type(element) == CLEARview.elements.CLEAR_DJ:
                #print ("J", name)
                Ix = self.getCur("DH"+name[1:])
                Iy = self.getCur("DV"+name[1:])
                element.update(Ix,Iy,self.P0)

    def getCur(self,coreName):
        return self.japc.getParam("CA."+coreName+"/Acquisition#currentAverage")

class CLEARonline_dummy(CLEARonline):
    "Dummy online model where all currents etc. are fixed"
        
    def __init__(self):
        CLEARonline.__init__(self)
    
    def update(self):
        "Hard-coded element currents (demonstration purposes)"
        super().update()
        
        self.lattice.elements['QFD0350'].update( 20.0, self.P0)
        self.lattice.elements['QDD0355'].update(-40.0, self.P0)
        self.lattice.elements['QFD0360'].update( 20.0, self.P0)
        
        self.lattice.elements['QFD0510'].update( 33.0, self.P0)
        self.lattice.elements['QDD0515'].update(-66.0, self.P0)
        self.lattice.elements['QFD0520'].update( 33.0, self.P0)
        
        self.lattice.elements['QFD0760'].update( 50.0, self.P0)
        self.lattice.elements['QDD0765'].update(-100.0, self.P0)
        self.lattice.elements['QFD0770'].update( 60.0, self.P0)

        self.lattice.elements['QDD0870'].update(-120.0, self.P0)
        self.lattice.elements['QFD0880'].update( 200.0, self.P0)
        
        self.lattice.elements['DJ0540'].update( -2, 2, self.P0)
        self.lattice.elements['DJ0590'].update(  2,-2, self.P0)
        
class CLEARonline_autoconverted(onlineModel):
    "Base class for the _online- and _dummy autoconverted CLEAR lattice classes"
    
    #Typical beam parameters
    P0 = 200.0 #[MeV/c]
    epsN_x = 10.0 #[um]
    epsN_y = 10.0 #[um]
    beta0_x = 2.0 #[m]
    beta0_y = 2.0 #[m]
    alpha0_x = 0.0
    alpha0_y = 0.0
    
    x0  = 0.0 #[mm]
    y0  = 0.0 #[1e-3]
    xp0 = 0.0 #[mm]
    yp0 = 0.0 #[1e-3]
    
    startingPoints_possible = ['START', 'CA.QS0350', 'CA.BTV0390', 'CA.QS0510', 'CA.BTV0620', 'CA.BTV0730', 'CA.QS0760', 'CA.BTV0810', 'CA.QS0870', 'CA.BTV0910']

    def __init__(self):
        onlineModel.__init__(self)
        import clearST_survey0_tfs #get the lattice
        self.lattice=clearST_survey0_tfs.buildLattice(self.P0)

class CLEARonline_autoconverted_online(CLEARonline_autoconverted):
    japc = None
    
    def __init__(self):
        CLEARonline_autoconverted.__init__(self)
        import pyjapc
        self.japc = pyjapc.PyJapc("SCT.USER.SETUP")
    
    def update(self):
        "Get the current magnet currents from japc"
        super().update()
        
        for name, element in self.lattice.elements.items():
            if type(element) == CLEARview.elements.CLEAR_Q:
                I = self.getCur(name)
                if name[4] == "F":
                    element.update(I,self.P0)
                elif name[4] == "D":
                    element.update(-I,self.P0)
                else:
                    print ("Strange quad name '{}'".format(name))
            elif type(element) == CLEARview.elements.CLEAR_DG120 or type(element) == CLEARview.elements.CLEAR_DJ:
                Ix = self.getCur(name)
                Iy = self.getCur(name.replace('DH','DV'))
                element.update(Ix,Iy,self.P0)

    def getCur(self,elemName):
        return self.japc.getParam(elemName+"/Acquisition#currentAverage")

class CLEARonline_autoconverted_dummy(CLEARonline_autoconverted):
    "Dummy online model where all currents etc. are fixed"
        
    def __init__(self):
        CLEARonline_autoconverted.__init__(self)
    
    def update(self):
        "Hard-coded element currents (demonstration purposes)"
        super().update()
        
        self.lattice.elements['CA.QFD0350'].update( 20.0, self.P0)
        self.lattice.elements['CA.QDD0355'].update(-40.0, self.P0)
        self.lattice.elements['CA.QFD0360'].update( 20.0, self.P0)
        
        self.lattice.elements['CA.QFD0510'].update( 33.0, self.P0)
        self.lattice.elements['CA.QDD0515'].update(-66.0, self.P0)
        self.lattice.elements['CA.QFD0520'].update( 33.0, self.P0)
        
        self.lattice.elements['CA.QFD0760'].update( 50.0, self.P0)
        self.lattice.elements['CA.QDD0765'].update(-100.0, self.P0)
        self.lattice.elements['CA.QFD0770'].update( 60.0, self.P0)

        self.lattice.elements['CA.QDD0870'].update(-120.0, self.P0)
        self.lattice.elements['CA.QFD0880'].update( 200.0, self.P0)
        
        self.lattice.elements['CA.DHJ0540'].update( -2, 2, self.P0)
        self.lattice.elements['CA.DHJ0590'].update(  2,-2, self.P0)
