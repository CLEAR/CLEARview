# -*- coding: utf-8 -*-

import sys
sys.path.append("../.")
import CLEARview

def buildCLEAR(P0):
    """
    Sequence used for building the CLEAR linac (experimental part only, not CALIFES).
    Based on clear.seqx
    
    P0: Reference momentun [MeV]
    """
    
    CLEARlattice = CLEARview.lattice.Lattice()
    
    ## Quadrupoles
    #Add the 3xx triplet (at negative s)
    # QFD350 was taken as Ref for measurements in the machine (DG 20.04.2017)
    # Note: The start of this element was going to end up at a negative position,
    # so the reference is taken at the start of the element to shift it forwards.
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'QFD0350', 0.0   , refer="start" )
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'QDD0355', 0.441 , refer="start" )
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'QFD0360', 0.8805, refer="start" )
    #Add the 5xx triplet
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'QFD0510', 4.020, referFrom="QFD0350")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'QDD0515', 4.520, referFrom="QFD0350")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'QFD0520', 5.020, referFrom="QFD0350")
    #Add the 7xx triplet
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'QFD0760', 11.827, referFrom="QFD0350")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'QDD0765', 12.277, referFrom="QFD0350")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'QFD0770', 12.727, referFrom="QFD0350")
    #Add the 8xx doublet
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'QDD0870', 2.855, referFrom="QFD0770")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'QFD0880', 0.450, referFrom="QDD0870")
    #Extra quad on in-air table:
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'QDD0920', 1.400, referFrom="QFD0880")

    ### Now let's do the correctors
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_DG120(0.0, 0.0, P0), 'DG0385', -0.113 + 1.276, referFrom="QFD0350")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_DJ(0.0, 0.0, P0), 'DJ0540', 5.646, referFrom="QFD0350")
    #This one I measured the position of (Kyrre 1/10-2018), measured end-to-end so add 2*(element_physical_length/2.0) 
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_DJ(0.0, 0.0, P0), 'DJ0590', 1.470+0.1, referFrom="DJ0540")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_DJ(0.0, 0.0, P0), 'DJ0710', 10.978, referFrom="QFD0350")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_DJ(0.0, 0.0, P0), 'DJ0780', 0.595, referFrom="QFD0770")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_DJ(0.0, 0.0, P0), 'DJ0840', 2.315, referFrom="QFD0770")
    
    ### Important markers
    ## Note: BTV390 is the point separating CLEAR and CALIFES in the MadX file
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'BTV0390', 1.215+0.5*0.226, referFrom="QFD0360")
    #CLIC structure and BPMs
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'ACS0640-START', 0.864+0.1/2.0, referFrom="DJ0590")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'ACS0650-END',   0.477, referFrom="ACS0640-START")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'BPC0660-MID',   0.125+0.08/2.0, referFrom="ACS0650-END")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'BPC0670-MID',   0.080+2*0.08/2.0, referFrom="BPC0660-MID")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'BPC0680-MID',   0.124+2*0.08/2.0, referFrom="BPC0670-MID")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'BPC0690-MID',   0.125+2*0.08/2.0, referFrom="BPC0680-MID")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'BTV0620',       -(0.035+0.105+0.146+0.070/2.0), referFrom="ACS0640-START")
    #More BTV
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'BTV0730', 11.397, referFrom="QFD0350")
    #Plasma lens stuff
    L_PLC = 0.03 #[m]
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'PLC800-S', 1.175, referFrom="QFD0770")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'BTV800', L_PLC/2.0 , referFrom="PLC800-S")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'PLC800-E', L_PLC, referFrom="PLC800-S")
    #More BTVs
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'BTV0810', 1.460, referFrom="QFD0770")
    #TODO: Measure properly the position of these -- taken from March 19th BEAM LINE MAP for the PLE
    # The in-air marker is pure guesswork (it's the old YAG position)
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'BTV0910', 1.120, referFrom="QFD0880.end")
    CLEARlattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'In-Air', 1.913, referFrom="QFD0880.end")
    
    CLEARlattice.implicitSeq_finalize()
    return CLEARlattice


if __name__ == "__main__":
    """
    A small '__main__' method to run the builder and show the result,
    very useful for debugging!
    """
    
    CLEAR = buildCLEAR(200.0)
    
    import matplotlib.pyplot as plt
    plt.figure()
    #plt.axhline(ls='--', color='k')
    #CLEARview.plot.vlines(CLEAR.elements)
    CLEARview.plot.addElemsToPlot(plt.gca(),CLEAR)
    CLEARview.plot.addElemNamesToPlot(plt.gca(), CLEAR)
    plt.xlabel('s [m]')
    plt.show()
