#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Autoconverted CLEARview lattice file via madx2lattice.py,
created from survey file 'clearST.survey0.tfs', input file metadata:
TITLE='no-title', DATE='26/04/23', TIME=12.13.59
"""

import sys
sys.path.append("../.")
import CLEARview

def buildLattice(P0, doFinalize=True):
    theLattice = CLEARview.lattice.Lattice();

    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.ACS0270S', 0.0)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.ACS0270S_MECH', 0.08650000000000091)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.BPC0310', 0.3265000000000029)
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_DG120(0.0, 0.0, P0), 'CA.DHG0320', 0.527000000000001, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.QS0350', 1.0435000000000016)
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'CA.QFD0350', 1.2695000000000007, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'CA.QDD0355', 1.709500000000002, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'CA.QFD0360', 2.1494999999999997, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_DG120(0.0, 0.0, P0), 'CA.DHG0385', 2.812000000000001, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.BTV0390', 3.3565000000000005)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.CALIFES$END', 3.3565000000000005)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.CLEAR$START', 3.3565000000000005)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.QS0510', 5.0135000000000005)
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'CA.QFD0510', 5.2395, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'CA.QDD0515', 5.7395, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'CA.QFD0520', 6.2395, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.BPM0530', 6.539500000000001)
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_DJ(0.0, 0.0, P0), 'CA.DHJ0540', 6.798500000000001, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_DJ(0.0, 0.0, P0), 'CA.DHJ0590', 8.025500000000001, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.BPM0595', 8.219500000000002)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.BTV0620', 8.694500000000001)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.BPM0690', 11.509500000000001)
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_DJ(0.0, 0.0, P0), 'CA.DHJ0710', 12.1325, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.BTV0730', 12.514500000000002)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.QS0760', 12.831500000000002)
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'CA.QFD0760', 13.057500000000001, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'CA.QDD0765', 13.5075, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'CA.QFD0770', 13.9575, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_DJ(0.0, 0.0, P0), 'CA.DHJ0780', 14.477500000000003, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.PLC0800M', 15.017499999999998)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.BTV0800', 15.017499999999998)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.BTV0810', 15.3095)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.BPM0820', 15.612500000000002)
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_DJ(0.0, 0.0, P0), 'CA.DHJ0840', 16.311500000000002, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.QS0870', 16.4865)
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'CA.QDD0870', 16.7125, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), 'CA.QFD0880', 17.1625, refer='end')
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.BPM0890', 17.290499999999998)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.CLEAR$END', 17.3495)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.CLDUMP$START', 17.3495)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.BTV0910', 18.2545)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.VAW0915', 18.3245)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.ICT0930', 18.3345)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.MTV_DUMP', 19.2845)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.CLDUMP$END', 19.4045)
    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), 'CA.STLINE$END', 19.4045)

    if doFinalize==True:
        theLattice.implicitSeq_finalize()

    return theLattice

if __name__ == "__main__":
    """
    A small '__main__' method to run the builder and show the result,
    very useful for debugging!
    """
    
    theLattice = buildLattice(200.0)
    
    import matplotlib.pyplot as plt
    plt.figure()
    #plt.axhline(ls='--', color='k')
    #CLEARview.plot.vlines(theLattice.elements)
    CLEARview.plot.addElemsToPlot(plt.gca(),theLattice)
    CLEARview.plot.addElemNamesToPlot(plt.gca(), theLattice)
    plt.xlabel('s [m]')
    plt.show()
