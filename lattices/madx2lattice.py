#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This file contains a utility for opening Mad-X SURVEY files,
and exporting a lattice Python program (.py file), which if needed can be manually edited further.

Note: This uses the MadxTools package: https://github.com/AcceleratorPhysicsUiO/MadXTools
"""

import numpy as np
import sys
import argparse
from madxtools import TableFS

sys.path.append("../.")
import CLEARview

ifname = None
firstElemName = None


parser = argparse.ArgumentParser(description="Convert a Mad-X SURVEY.tfs file to a CLEARview lattice")
parser.add_argument("surveyFileName", type=str, help="Input file name")
parser.add_argument('--firstElementName', required=False, help="Name of first element to include")
parser.add_argument('--machine', choices=["CLEAR"], required=False, help="Use machine-specific conversions")
parser.add_argument('--outputFileName', type=str,required=False, help="Output file name, default is inputfilename.txt")
parsed = parser.parse_args()
ifname        = parsed.surveyFileName
firstElemName = parsed.firstElementName
machineName   = parsed.machine
if parsed.outputFileName == None:
    # Creates a .txt file by default,
    # to make the accidental overwriting of
    # a manually modified lattice file less likely
    ofname = ifname.replace('.','_')+".txt"
else:
    ofname = parsed.outputFileName

ifile = TableFS(ifname)
ifile.convertToNumpy()

if ifile.metaData["TYPE"] != "SURVEY":
    print("Error: Expected input file to be of type SURVEY")
if not (("NAME" in ifile.Data) and ("KEYWORD" in ifile.Data) and ("L" in ifile.Data) and ("S" in ifile.Data)):
    print("Expect input file to contain columns 'NAME', 'KEYWORD', 'L' and 'S'. Please check your SELECT statement!")

ofile = open(ofname,'w')
ofile.write("#!/usr/bin/env python3\n")
ofile.write("# -*- coding: utf-8 -*-\n\n")

ofile.write(f'"""\n')
ofile.write(f"Autoconverted CLEARview lattice file via madx2lattice.py,\n")
ofile.write(f"created from survey file '{ifname}', input file metadata:\n")
ofile.write(f"TITLE='{ifile.metaData['TITLE']}', DATE='{ifile.metaData['DATE']}', TIME={ifile.metaData['TIME']}\n")
ofile.write('"""\n')

ofile.write("""
import sys
sys.path.append("../.")
import CLEARview

def buildLattice(P0, doFinalize=True):
    theLattice = CLEARview.lattice.Lattice();

""")

#print(ifile.Data)

sStart = 0.0
started = True
if firstElemName is None:
    started = True
else:
    started = False

for i in range(ifile.nLines):
    if ifile.Data["NAME"][i] == firstElemName:
        sStart = ifile.Data["S"][i]
        started = True
    if not started:
        continue

    if ifile.Data["KEYWORD"][i] == "DRIFT":
        continue

    elif ifile.Data["KEYWORD"][i] == "QUADRUPOLE":
        L = ifile.Data["L"][i]
        defaultQuadLine = f"    theLattice.implicitSeq_addElem( CLEARview.elements.QuadThick({L}, 0.0), '{ifile.Data['NAME'][i]}', {str(ifile.Data['S'][i]-sStart)}, refer='end')\n"
        if machineName == None:
            ofile.write(defaultQuadLine)
        elif machineName == "CLEAR":
            if L == CLEARview.elements.CLEAR_Q.L_quad:
                ofile.write(f"    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(0.0, P0), '{ifile.Data['NAME'][i]}', {str(ifile.Data['S'][i]-sStart)}, refer='end')\n")
            else:
                print(f"Warning, converted {ifile.Data['NAME'][i]} to default thick quadrupole, wrong length for CLEAR_Q")
                ofile.write(defaultQuadLine)
        else:
            print(f"Unrecognized machine name {machineName} in quad converter")
            exit(1)

    elif ifile.Data["KEYWORD"][i] == "KICKER":
        L = ifile.Data["L"][i]
        defaultKickerLine = f"    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_DJ({ifile.Data['L'][i]}, 0.0, 0.0), '{ifile.Data['NAME'][i]}', {str(ifile.Data['S'][i]-sStart)}, refer='end')\n"
        if machineName == None:
            ofile.write(defaultKickerLine)
        elif machineName == "CLEAR":
            if ifile.Data["NAME"][i].startswith("CA.DHJ"):
                ofile.write(f"    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_DJ(0.0, 0.0, P0), '{ifile.Data['NAME'][i]}', {str(ifile.Data['S'][i]-sStart)}, refer='end')\n")
            elif ifile.Data["NAME"][i].startswith("CA.DHG"):
                ofile.write(f"    theLattice.implicitSeq_addElem( CLEARview.elements.CLEAR_DG120(0.0, 0.0, P0), '{ifile.Data['NAME'][i]}', {str(ifile.Data['S'][i]-sStart)}, refer='end')\n")
            else:
                print(f"Warning, converted {ifile.Data['NAME'][i]} to default thick kicker, wrong name or length for CLEAR_DJ and CLEAR_DG120")
                ofile.write(defaultKickerLine)
        else:
            print(f"Unrecognized machine name {machineName} in kicker converter")
            exit(1)

    elif ifile.Data["KEYWORD"][i] == "MARKER" or ifile.Data["KEYWORD"][i] == "MONITOR":
        L = ifile.Data["L"][i]
        if L > 0.0:
            print(f"Warning: Maker/monitor {ifile.Data['NAME'][i]} has L={ifile.Data['L'][i]}>0.0, placing CLEARview marker in the middle of element")
        ofile.write("    theLattice.implicitSeq_addElem( CLEARview.elements.Marker(), '" + ifile.Data["NAME"][i] + "', " + str(ifile.Data["S"][i]-sStart-ifile.Data["L"][i]/2.0) + ")\n")


ofile.write("""
    if doFinalize==True:
        theLattice.implicitSeq_finalize()

    return theLattice
""")

ofile.write("""
if __name__ == "__main__":
    \"""
    A small '__main__' method to run the builder and show the result,
    very useful for debugging!
    \"""
    
    theLattice = buildLattice(200.0)
    
    import matplotlib.pyplot as plt
    plt.figure()
    #plt.axhline(ls='--', color='k')
    #CLEARview.plot.vlines(theLattice.elements)
    CLEARview.plot.addElemsToPlot(plt.gca(),theLattice)
    CLEARview.plot.addElemNamesToPlot(plt.gca(), theLattice)
    plt.xlabel('s [m]')
    plt.show()
""")

ofile.close()

print(f"Wrote file '{ofname}' based on '{ifname}'.")
