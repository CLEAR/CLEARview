#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append("../.")
import CLEARview

import numpy as np

import matplotlib.pyplot as plt

#Define the lattice -- just a 3m drift, split in 3 with a zero-kick kicker in the middle.

kickLattice = CLEARview.lattice.Lattice()
kickLattice.addElem( CLEARview.elements.Drift(1.0) , 'drift1')
kickLattice.addElem( CLEARview.elements.KickerThick(1.0, 0.0, 0.0) , 'kicker1')
kickLattice.addElem( CLEARview.elements.Drift(1.0) , 'drift2')

# Track a single particle
x0 = np.asarray([1e-4,5e-5])
y0 = np.asanyarray([-2e-4,10e-5])
(s,x,y) = kickLattice.computeTrajectory( x0, y0 )
# Plot it
plt.figure()
plt.plot(s,x[0,:]*1e3, label='x')
plt.plot(s,y[0,:]*1e3, label='y')
plt.legend(loc=0)
plt.xlabel('s [m]')
plt.ylabel('pos [mm]')
CLEARview.plot.vlines(s)
plt.axhline(0.0, ls='--',color='k')

CLEARview.plot.addElemsToPlot(plt.gca(),kickLattice)

print("tst")
plt.plot()
