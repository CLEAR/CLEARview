#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append("../.")
import CLEARview

import numpy as np

import matplotlib.pyplot as plt


# Defining the lattice (just a simple triplet,
# which is matched to the distribution given below)
# An extra observation plane is made to make the focus point more obvious.
simpleTriplet = CLEARview.lattice.Lattice()

simpleTriplet.addElem(CLEARview.elements.Drift(1.0),                  'drift1')
simpleTriplet.addElem(CLEARview.elements.QuadThick(0.2,  3.7),        'QF1')
simpleTriplet.addElem(CLEARview.elements.Drift(0.3),                  'drift2')
simpleTriplet.addElem(CLEARview.elements.QuadThick(0.2, -7.5),        'QD1')
simpleTriplet.addElem(CLEARview.elements.Drift(0.3),                  'drift3')
simpleTriplet.addElem(CLEARview.elements.QuadThick(0.2,  4.8),        'QF2')
simpleTriplet.addElem(CLEARview.elements.Drift(2.0),                  'drift4')
simpleTriplet.addElem(CLEARview.elements.Drift(1.0),                  'drift5')
#More stuff: Kickers!
simpleTriplet.addElem(CLEARview.elements.KickerThick(0.1,2e-3,-5e-4), 'kicker1')
simpleTriplet.addElem(CLEARview.elements.Drift(1.0),                  'drift6')
simpleTriplet.addElem(CLEARview.elements.KickerThick(0.1,-2e-3,5e-4), 'kicker2')
simpleTriplet.addElem(CLEARview.elements.Drift(1.0),                  'drift7')
#And some quads
simpleTriplet.addElem(CLEARview.elements.QuadThick(0.2,  5.5),        'QF3')
simpleTriplet.addElem(CLEARview.elements.Drift(0.3),                  'drift8')
simpleTriplet.addElem(CLEARview.elements.QuadThick(0.2,  -9.3),       'QD2')
simpleTriplet.addElem(CLEARview.elements.Drift(0.3),                  'drift9')
simpleTriplet.addElem(CLEARview.elements.QuadThick(0.2,  5.5),        'QF4')
simpleTriplet.addElem(CLEARview.elements.Drift(2.0),                  'drift10')
simpleTriplet.addElem(CLEARview.elements.Drift(1.0),                  'drift11')

# Track a single particle
x0 = np.asarray([1e-4,5e-5])
y0 = np.asanyarray([-2e-4,10e-5])
(s,x,y) = simpleTriplet.computeTrajectory( x0, y0 )
# Plot it
plt.figure()
plt.plot(s,x[0,:]*1e3, label='x')
plt.plot(s,y[0,:]*1e3, label='y')
plt.legend(loc=0)
plt.xlabel('s [m]')
plt.ylabel('pos [mm]')
CLEARview.plot.vlines(s)
plt.axhline(0.0, ls='--',color='k')

##
## Make a distribution and track it
##
Npart = 200

eps0  = 1.0
E0 = 200.0

beta0 = 50.0
alpha0 = -3.0

x0Dist = CLEARview.opticsUtils.makeDist(eps0, beta0, alpha0, Npart, E0)
y0Dist = CLEARview.opticsUtils.makeDist(eps0, beta0, alpha0, Npart, E0)
# Test -- do we get the same values out?
CLEARview.opticsUtils.printTwissDist(x0Dist,E0)
print()

# Track!
(s,xDist,yDist) = simpleTriplet.computeTrajectory( x0Dist, y0Dist )

CLEARview.plot.tracks(s,xDist,yDist)

##
## Let's try with twiss functions!
##
xt0 = np.asarray([beta0,alpha0,CLEARview.opticsUtils.getGamma(alpha0,beta0)])
yt0 = np.asarray([beta0,alpha0,CLEARview.opticsUtils.getGamma(alpha0,beta0)])

(s, xt,yt, xTra,yTra) = simpleTriplet.computeTwiss(xt0,yt0)
CLEARview.plot.alphaBeta(s,xt,yt)

### Comparing TWISS to TWISS-through-tracking
#for i in range(len(s)):
#    print("s         =",s[i],"[m]")
#    print("beta      =",xt[0,i], "[m]")
#    print("alpha     =",xt[1,i])
#    print("gamma     =",xt[2,i], "[1/m]")
#    print("gamma_alt =",CLEARview.opticsUtils.getGamma(xt[1,i],xt[0,i]), "[1/m]")
#    
#    #print(xDist[:,:,i].T)
#    CLEARview.opticsUtils.printTwissDist(xDist[:,:,i].T,E0)
#    print()


## Let's plot the beam sigmas!
CLEARview.plot.sigmas(s,xt,yt,CLEARview.opticsUtils.get_eps_g(eps0,E0), xTra,yTra)

## Sigmas with tracks
(ax1,ax2) = CLEARview.plot.sigmas(s,xt,yt,CLEARview.opticsUtils.get_eps_g(eps0,E0), xTra,yTra)
CLEARview.plot.tracks_singleplane(s,xDist,ax1,ylab='x')
CLEARview.plot.tracks_singleplane(s,yDist,ax2,ylab='y')

plt.show()