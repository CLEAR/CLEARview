# -*- coding: utf-8 -*-

import sys
sys.path.append("../.")
import CLEARview

import numpy as np

import matplotlib.pyplot as plt

#Define the lattice
P0 = 215.0       # [MeV/c]
E0 = P0          # yeah whatever

L_quad = CLEARview.elements.CLEAR_Q.L_quad   # quadrupole magnetic length [m]
dist_quad = 0.45 # Quadrupole distances, centre-to-centre

L_DG120 = CLEARview.elements.CLEAR_DG120.L_corr # corrector type DG120 magnetic length [m]

CLEARlattice = CLEARview.lattice.Lattice()
# Lattice only has superficial similarities to the CLEAR one
CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q( 80.0, P0), 'QF0350', 0.0, refer='start')
CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q(- 80.0, P0), 'QF0355', L_quad+dist_quad, refer='start')
CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_Q( 80.0, P0), 'QF0360', 2*(L_quad+dist_quad), refer='start')
CLEARlattice.implicitSeq_addElem( CLEARview.elements.CLEAR_DG120(10.0,-20.0,P0), 'DG0225', 0.5, referFrom='QF0360.end')
CLEARlattice.implicitSeq_addElem( CLEARview.elements.Element(), 'MRK1', 1.0, referFrom='DG0225.end')
CLEARlattice.implicitSeq_finalize()

# More beam parameters
eps0  = 25.0

beta0_x  = 12.0
alpha0_x = -1.75

beta0_y  = 28.0
alpha0_y = -2.48

SUBDIVIDE=0.01 #[m]

print("*** TWISS ***")
xt0 = np.asarray([beta0_x,alpha0_x,CLEARview.opticsUtils.getGamma(alpha0_x,beta0_x)])
yt0 = np.asarray([beta0_y,alpha0_y,CLEARview.opticsUtils.getGamma(alpha0_y,beta0_y)])

(s2, xt2,yt2, xTra2,yTra2, sSub2,xtSub2,ytSub2,xTraSub2,yTraSub2) =\
    CLEARlattice.computeTwiss(xt0,yt0, subdivide=SUBDIVIDE)

(ax1,ax2) = CLEARview.plot.alphaBeta(s2,xtSub2,ytSub2,sSub2)
CLEARview.plot.addElemsToPlot(ax1,CLEARlattice)
CLEARview.plot.addElemsToPlot(ax2,CLEARlattice)
ax1.set_title(r"Subdivided at $\Delta s$={} [m]".format(SUBDIVIDE))
CLEARview.plot.addElemNamesToPlot(ax1,CLEARlattice)

(ax1,ax2) = CLEARview.plot.sigmas(s2,xtSub2,ytSub2,CLEARview.opticsUtils.get_eps_g(eps0,E0), xTraSub2,yTraSub2, sSub=sSub2)
CLEARview.plot.addElemsToPlot(ax1,CLEARlattice)
CLEARview.plot.addElemsToPlot(ax2,CLEARlattice)
ax1.set_title(r"Subdivided at $\Delta s$={} [m]".format(SUBDIVIDE))
CLEARview.plot.addElemNamesToPlot(ax1,CLEARlattice)

plt.show()
