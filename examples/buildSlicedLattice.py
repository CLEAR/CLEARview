#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append("../.")
import CLEARview

import numpy as np

import matplotlib.pyplot as plt

#Define the lattice

P0 = 215.0       # [MeV/c]
E0 = P0          # yeah whatever

L_quad = CLEARview.elements.CLEAR_Q.L_quad   # quadrupole magnetic length [m]
dist_quad = 0.45 # Quadrupole distances, centre-to-centre

L_DG120 = CLEARview.elements.CLEAR_DG120.L_corr # corrector type DG120 magnetic length [m]

CLEARlattice = CLEARview.lattice.Lattice()

CLEARlattice.addElem( CLEARview.elements.CLEAR_Q( 80.0,P0), 'QF0350')
CLEARlattice.addElem( CLEARview.elements.Drift(dist_quad-L_quad),  'Q03xx_drift1')
CLEARlattice.addElem( CLEARview.elements.CLEAR_Q(-120.0,P0), 'QF0355')
CLEARlattice.addElem( CLEARview.elements.Drift(dist_quad-L_quad),  'Q03xx_drift2')
CLEARlattice.addElem( CLEARview.elements.CLEAR_Q(  40,P0), 'QF0360')

CLEARlattice.addElem( CLEARview.elements.Drift(1.622 - 3*L_quad - 2*(dist_quad-L_quad) - L_DG120/2.0),  'Q3xx-D385_drift')
CLEARlattice.addElem( CLEARview.elements.CLEAR_DG120(10.0,-20.0,P0), 'DG0225')
CLEARlattice.addElem( CLEARview.elements.Drift(2.251 - 3*L_quad - 2*(dist_quad-L_quad) - (1.622 - 3*L_quad - 2*(dist_quad-L_quad)) - L_DG120),  'D385-BTV0390_drift')
CLEARlattice.addElem( CLEARview.elements.Drift(0.5),  'drift')
#CLEARlattice.addElem( CLEARview.elements.Drift(2.251 - 3*L_quad - 2*(dist_quad-L_quad) - CLEARlattice.getElemS('DG0225',True)),  'D385-BTV0390_drift')


eps0  = 25.0

beta0_x  = 12.0
alpha0_x = -1.75

beta0_y  = 28.0
alpha0_y = -2.48

SUBDIVIDE=0.01 #[m]

#TRACK
print("*** TRACK ***")
Npart = 200
x0Dist = CLEARview.opticsUtils.makeDist(eps0, beta0_x, alpha0_x, Npart, E0)
y0Dist = CLEARview.opticsUtils.makeDist(eps0, beta0_y, alpha0_y, Npart, E0)
(s1,xDist1,yDist1) = CLEARlattice.computeTrajectory( x0Dist, y0Dist )

(s2,xDist2,yDist2, sSub2,xDistSub2,yDistSub2) = CLEARlattice.computeTrajectory( x0Dist, y0Dist, subdivide=SUBDIVIDE)

(ax1,ax2) = CLEARview.plot.tracks(s1,xDist1,yDist1)
CLEARview.plot.addElemsToPlot(ax1,CLEARlattice)
CLEARview.plot.addElemsToPlot(ax2,CLEARlattice)
ax1.set_title("Tracking, no subdivisions")
CLEARview.plot.addElemNamesToPlot(ax2,CLEARlattice,ypos=0.4)

(ax1,ax2) = CLEARview.plot.tracks(s2,xDistSub2,yDistSub2,sSub=sSub2)
CLEARview.plot.addElemsToPlot(ax1,CLEARlattice)
CLEARview.plot.addElemsToPlot(ax2,CLEARlattice)
ax1.set_title(r"Tracking, subdivided at $\Delta s$={} [m]".format(SUBDIVIDE))
CLEARview.plot.addElemNamesToPlot(ax2,CLEARlattice,ypos=0.4)

#TWISS
print("*** TWISS ***")
xt0 = np.asarray([beta0_x,alpha0_y,CLEARview.opticsUtils.getGamma(alpha0_x,beta0_x)])
yt0 = np.asarray([beta0_y,alpha0_y,CLEARview.opticsUtils.getGamma(alpha0_y,beta0_y)])

(s1, xt1,yt1, xTra1,yTra1) = CLEARlattice.computeTwiss(xt0,yt0)

(s2, xt2,yt2, xTra2,yTra2, sSub2,xtSub2,ytSub2,xTraSub2,yTraSub2) =\
    CLEARlattice.computeTwiss(xt0,yt0, subdivide=SUBDIVIDE)

#print("s=")
#print(s1)
#print(s2)
#print("sSub=")
#print(sSub2)
#print("xt=")
#print(xt1)
#print(xt2)
#print("xtSub=")
#print(xtSub2)

(ax1,ax2) = CLEARview.plot.alphaBeta(s1,xt1,yt1)
CLEARview.plot.addElemsToPlot(ax1,CLEARlattice)
CLEARview.plot.addElemsToPlot(ax2,CLEARlattice)
ax1.set_title("No subdivisions")
CLEARview.plot.addElemNamesToPlot(ax1,CLEARlattice)

(ax1,ax2) = CLEARview.plot.alphaBeta(s2,xtSub2,ytSub2,sSub2)
CLEARview.plot.addElemsToPlot(ax1,CLEARlattice)
CLEARview.plot.addElemsToPlot(ax2,CLEARlattice)
ax1.set_title(r"Subdivided at $\Delta s$={} [m]".format(SUBDIVIDE))
CLEARview.plot.addElemNamesToPlot(ax1,CLEARlattice)


(ax1,ax2) = CLEARview.plot.sigmas(s1,xt1,yt1,CLEARview.opticsUtils.get_eps_g(eps0,E0), xTra1,yTra1)
CLEARview.plot.addElemsToPlot(ax1,CLEARlattice)
CLEARview.plot.addElemsToPlot(ax2,CLEARlattice)
ax1.set_title("No subdivisions")
CLEARview.plot.addElemNamesToPlot(ax1,CLEARlattice)


(ax1,ax2) = CLEARview.plot.sigmas(s2,xtSub2,ytSub2,CLEARview.opticsUtils.get_eps_g(eps0,E0), xTraSub2,yTraSub2, sSub=sSub2)
CLEARview.plot.addElemsToPlot(ax1,CLEARlattice)
CLEARview.plot.addElemsToPlot(ax2,CLEARlattice)
ax1.set_title(r"Subdivided at $\Delta s$={} [m]".format(SUBDIVIDE))
CLEARview.plot.addElemNamesToPlot(ax1,CLEARlattice)

plt.show()